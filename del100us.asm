  ; FILE delay.c
			;// Datei: delay.c
			;// Verzögerungsschleifen
			;//
			;// Holger Klabunde 
			;// 01.05.2001
			;// Compiler CC5x
			;
			;//#include "prozess.h"      /* Der verwendete Prozessor */
			;#include "protos.h"
			;
			;//Für Taktfrequenz des PIC = 4MHz
			;//Ein Taktzyklus=1us
			;//delay<=255 !!
			;unsigned char del;
			;
			;//Assemblerbefehle zwischen #asm und #endasm !! klein !! schreiben
			;//Kollidiert sonst mit inline.h
			;

;	$Revision: 0.2 $  $Date: 2005/11/20 01:57:22 $

		include		Processor.inc
		global		Delay100us

DELS		udata_ovr
;delay_2     EQU   0x0F
delay_2		res		1
;del         EQU   0x1D
del			res		1
;delay       EQU   0x12	;*****************
;delay		res		1

			;void Delay1ms(unsigned char delay)
			;{
DEL		code
			;
			;void Delay100us(unsigned char delay)
			;{
Delay100us
	movwf delay_2
			; del=delay;
	movf  delay_2,W
	movwf del
			;#asm
			;
			;DLUS4M1
			;   movlw 0x19          //1 Wert 25
m010
  IFDEF KHZ32
    movlw   .1  ; needs to be 0.2
  ELSE
	movlw .25
  ENDIF
			;   movwf FSR           //1
	movwf FSR
			;
			;DLUS4M2                //4 cycles
			;     nop               //1
m011	nop  
			;     decfsz FSR        //1
	decfsz FSR,F
			;     goto  DLUS4M2     //2
	goto  m011
			;
			;   decfsz del     //1
	decfsz del,F
			;   goto DLUS4M1        //2
	goto  m010
			;#endasm
			;}
	return
			;
			;//2 Cycles for CAll
			;//2 Cycles for RET
	end
