; Processor.inc - Select include based on processor symbol
;  $Revision: 1.40 $ $Date: 2005/11/20 16:43:55 $
		nolist
;**
;  This file includes the appropriate procesor include file
;  based on the processor symbol passed in from the IDE.
;
;  20-May-2014 Updated to include important registers for common
;  families: e.g. CMCON in PIC16F627,628, etc.
;**
;  WB8RCR - 23-Nov-04

;  18 pin parts

		IFDEF		__16F83
		include		p16f83.inc
		nolist
#define PROC 84
		ENDIF

		IFDEF		__16F84
		include		p16f84.inc
		nolist
#define PROC 84
		ENDIF

		IFDEF		__16F84A
		include		p16f84a.inc
		nolist
#define PROC 84
		ENDIF

		IFDEF		__16F627
		include		p16f627.inc
		nolist
#define PROC 627
#define HASCMCON
		ENDIF

		IFDEF		__16F627A
		include		p16f627a.inc
		nolist
#define PROC 627
#define HASCMCON
		ENDIF

		IFDEF		__16F628
		include		p16f628.inc
		nolist
#define PROC 627
#define HASCMCON
		ENDIF

		IFDEF		__16F628A
		include		p16f628a.inc
		nolist
#define PROC 627
#define HASCMCON
		ENDIF

		IFDEF		__16F648A
		include		p16f648a.inc
		nolist
#define PROC 627
#define HASCMCON
		ENDIF

		IFDEF		__16F88
		include		p16f88.inc
		nolist
#define PROC 88
; Note that although 87/88 has ADCON0/1 registers, these do not affect
; which pins are analog; that is determined by ANSEL
#define HASCMCON
#define HASANSEL
#define HASCCPCON
		ENDIF

		IFDEF		__16F87
		include		p16f87.inc
		nolist
#define PROC 88
#define HASCMCON
#define HASCCPCON
		ENDIF

		IFDEF		__16F819
		include		p16f819.inc
		nolist
#define PROC 819
#define HASADCON
#define HASCCPCON
		ENDIF

		IFDEF		__16F818
		include		p16f818.inc
		nolist
#define PROC 819
#define HASADCON
#define HASCCPCON
		ENDIF

		IFDEF		__16F716
		include		p16f716.inc
		nolist
#define PROC 716
#define HASADCON
#define HASCCPCON
		ENDIF

;	28 pin parts

		IFDEF		__16F872
		include		p16f872.inc
		nolist
#define PROC 872
#define HASADCON
#define HASCCPCON
		ENDIF

		IFDEF		__16F873
		include		p16f873.inc
		nolist
#define PROC 872
#define HASADCON
#define HASCCPCON
		ENDIF

		IFDEF		__16F876
		include		p16f876.inc
		nolist
#define PROC 872
#define HASADCON
#define HASCCPCON
		ENDIF

;	40 pin parts

		IFDEF		__16F874
		include		p16f874.inc
		nolist
#define PROC 872
#define HASADCON
#define HASCCPCON
		ENDIF

		IFDEF		__16F874A
		include		p16f874a.inc
		nolist
#define PROC 872
#define HASADCON
#define HASCCPCON
		ENDIF

		IFDEF		__16F877
		include		p16f877.inc
		nolist
#define PROC 872
#define HASADCON
#define HASCCPCON
		ENDIF

		IFDEF		__16F877A
		include		p16f877a.inc
		nolist
#define PROC 872
#define HASADCON
#define HASCCPCON
		ENDIF


		list

