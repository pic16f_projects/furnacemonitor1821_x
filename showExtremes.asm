            include     Processor.inc
            radix       DEC

            global      showExtremes

            extern      tempbuf,tempbuf2
            extern      min1,min2
            extern      max1,max2
            extern      delta
            extern      mindelta,maxdelta

            extern		LCDWriteByte,LCDCls,LCDPos,LCDInit
            extern		ShowBCD
            extern      Delay1ms
            extern      zeile

PROG        code

showExtremes:

			;
			;                       LCDInit(); //LCD Anzeige einstellen
            call        LCDInit
			;
            call        LCDCls

            ;   Maximum ambient
            movlw       .1
            movwf       zeile
            movlw       .2
            call        LCDPos
            movf        max1+1,W
            call        ShowBCD
            movf        max1,W
            call        ShowBCD

            ;   Maximum pipe
            movlw       .1
            movwf       zeile
            movlw       .8
            call        LCDPos
            movf        max2+1,W
            call        ShowBCD
            movf        max2,W
            call        ShowBCD

            ; Maximum Delta
            movlw       .1
            movwf       zeile
            movlw       .14
            call        LCDPos
            movf        maxdelta+1,W
            call        ShowBCD
            movf        maxdelta,W
            call        ShowBCD

            ;   Minimum ambient
            movlw       .2
            movwf       zeile
            movlw       .2
            call        LCDPos
            movf        min1+1,W
            call        ShowBCD
            movf        min1,W
            call        ShowBCD

            ;   Minimum pipe
            movlw       .2
            movwf       zeile
            movlw       .8
            call        LCDPos
            movf        min2+1,W
            call        ShowBCD
            movf        min2,W
            call        ShowBCD

            ; Minimum Delta
            movlw       .2
            movwf       zeile
            movlw       .14
            call        LCDPos
            movf        mindelta+1,W
            call        ShowBCD
            movf        mindelta,W
            call        ShowBCD

            return
            end


