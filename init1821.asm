;
            include     Processor.inc
            include     DS1821.inc
            radix       DEC

            global      init1821
		    extern		MasterReset,PollStatus,SendByte,ReadByte
            extern      Delay1ms
            ;extern      tempmin,tempmax

IO          code

init1821:
			;                       MasterReset();
            call        MasterReset
			;                       SendByte(WRITESTATUS);
            movlw       WRITESTATUS
            call        SendByte
			;                       SendByte(0x41); //One Shot Operation einstellen
            movlw       DSCONFIG | DS1SHOT
            call        SendByte
			;                       Delay1ms(255);  //Etwas warten
            movlw       .255
            call        Delay1ms
			;
			;                       tempmin=(long)12500;
            ;movlw       .212
            ;movlw       LOW(12500)
            ;movwf       tempmin
            ;movlw       .48
            ;movlw       HIGH(12500)
            ;movwf       tempmin+1
			;                       tempmax=(long)-5500;
            ;movlw       .132
            ;movwf       tempmax
            ;movlw       .234
            ;movwf       tempmax+1
            return
            end


