; 16-bit division from CC5X library

;	$Revision: 0.2 $  $Date: 2005/11/20 01:56:11 $

		include		Processor.inc
		global		_divS16_16
		global		arg1_8,arg2_8

		udata
;rm_4        EQU   0x11
rm_4		res		2
;counter_8   EQU   0x13
counter_8	res		1
;sign_3      EQU   0x14
sign_3		res		1

SHSP		udata_ovr
;arg1_8      EQU   0x0D
arg1_8		res		2
;arg2_8      EQU   0x0F
arg2_8		res		2

LIB		code
			;
			;int16 operator/ _divS16_16( int16 arg1, int16 arg2)
			;{
_divS16_16
			;    uns16 rm = 0;
	clrf  rm_4
	clrf  rm_4+1
			;    char counter = sizeof(arg1)*8+1;
	movlw .17
	movwf counter_8
			;    char sign = arg1.high8 ^ arg2.high8;
	movf  arg2_8+1,W
	xorwf arg1_8+1,W
	movwf sign_3
			;    if (arg1 < 0)  {
	btfss arg1_8+1,7
	goto  m002
			;       INVERT:
			;        arg1 = -arg1;
m001	comf  arg1_8+1,F
	comf  arg1_8,F
	incf  arg1_8,F
	btfsc STATUS,Z
	incf  arg1_8+1,F
			;        if (!counter)
	movf  counter_8,F
	btfss STATUS,Z
	goto  m002
			;            return arg1;
	movf  arg1_8,W
	return
			;    }
			;    if (arg2 < 0)
m002	btfss arg2_8+1,7
	goto  m003
			;        arg2 = -arg2;
	comf  arg2_8+1,F
	comf  arg2_8,F
	incf  arg2_8,F
	btfsc STATUS,Z
	incf  arg2_8+1,F
			;    goto ENTRY;
m003	goto  m006
			;    do  {
			;        rm = rl( rm);
m004	rlf   rm_4,F
	rlf   rm_4+1,F
			;        W = rm.low8 - arg2.low8;
	movf  arg2_8,W
	subwf rm_4,W
			;        genSubW( rm.high8, arg2.high8);
	movf  arg2_8+1,W
	btfss STATUS,C
	incfsz arg2_8+1,W
	subwf rm_4+1,W
			;        if (!Carry)
	btfsc STATUS,C
	goto  m005
			;            goto ENTRY;
	goto  m006
			;        rm.high8 = W;
m005	movwf rm_4+1
			;        rm.low8 -= arg2.low8;
	movf  arg2_8,W
	subwf rm_4,F
			;        Carry = 1;
	bsf   STATUS,C
			;       ENTRY:
			;        arg1 = rl( arg1);
m006	rlf   arg1_8,F
	rlf   arg1_8+1,F
			;        counter = decsz(counter);
	decfSZ counter_8,F
			;    } while (1);
	goto  m004
			;    if (sign & 0x80)
	btfss sign_3,7
	goto  m007
			;        goto INVERT;
	goto  m001
			;    return arg1;
m007	movf  arg1_8,W
	return
	end
