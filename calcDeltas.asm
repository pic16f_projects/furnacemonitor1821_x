            include     Processor.inc
            radix       DEC

            global      calcDeltas

            extern      tempbuf,tempbuf2
            extern      min1,min2
            extern      max1,max2
            extern      delta
            extern      mindelta,maxdelta

            udata
scratch     res         1

PROG        code
calcDeltas:

;
;   Calculate difference between pipe and ambient temperatures
;
;           .line   29; "calcDeltas.c"      delta = tempbuf - tempbuf2;
            banksel     tempbuf
            movf        tempbuf2,W
            subwf       tempbuf,W
            movwf       delta
            movf        (tempbuf2 + 1),W
            btfss       STATUS,0
            incf        (tempbuf2 + 1),W
            subwf       (tempbuf + 1),W
            movwf       (delta + 1)

;
;   Track maximum difference in temperature
;
;           .line   31; "calcDeltas.c"      if ( delta > maxdelta )  maxdelta = delta;
            movf        (maxdelta + 1),W
            addlw       0x80
            movwf       scratch
            movf        (delta + 1),W
            addlw       0x80
            subwf       scratch,W
            btfss       STATUS,2
            goto        skip01
            movf        delta,W
            subwf       maxdelta,W
skip01
            btfss       STATUS,0
            goto        skip02
            goto        skip03
skip02
            movf        delta,W
            movwf       maxdelta
            movf        (delta + 1),W
            movwf       (maxdelta + 1)
skip03

;
;   Track minimum difference in temperature
;
;           .line   33; "calcDeltas.c"      if ( delta < mindelta )  mindelta = delta;
            movf    (delta + 1),W
            addlw   0x80
            movwf   scratch
            movf    (mindelta + 1),W
            addlw   0x80
            subwf   scratch,W
            btfss   STATUS,2
            goto    skip04
            movf    mindelta,W
            subwf   delta,W
skip04
            btfss   STATUS,0
            goto    skip05
            goto    skip06
skip05
            movf    delta,W
            movwf   mindelta
            movf    (delta + 1),W
            movwf   (mindelta + 1)
skip06


;
;   Calculate maximum ambient temperature
;
;           .line   31; "calcDeltas.c"      if ( tempbuf > max1 )  max1 = tempbuf;
            movf        (max1 + 1),W
            addlw       0x80
            movwf       scratch
            movf        (tempbuf + 1),W
            addlw       0x80
            subwf       scratch,W
            btfss       STATUS,2
            goto        skip07
            movf        tempbuf,W
            subwf       max1,W
skip07
            btfss       STATUS,0
            goto        skip08
            goto        skip09
skip08
            movf        tempbuf,W
            movwf       max1
            movf        (tempbuf + 1),W
            movwf       (max1 + 1)
skip09

;
;   Calculate minimum ambient temperature
;
;           .line   33; "calcDeltas.c"      if ( tempbuf < min1 )  min1 = tempbuf;
            movf    (tempbuf + 1),W
            addlw   0x80
            movwf   scratch
            movf    (min1 + 1),W
            addlw   0x80
            subwf   scratch,W
            btfss   STATUS,2
            goto    skip10
            movf    min1,W
            subwf   tempbuf,W
skip10
            btfss   STATUS,0
            goto    skip11
            goto    skip12
skip11
            movf    tempbuf,W
            movwf   min1
            movf    (tempbuf + 1),W
            movwf   (min1 + 1)
skip12

;
;   Calculate maximum pipe temperature
;
;           .line   31; "calcDeltas.c"      if ( tempbuf2 > max2 )  max2 = tempbuf2;
            movf        (max2 + 1),W
            addlw       0x80
            movwf       scratch
            movf        (tempbuf2 + 1),W
            addlw       0x80
            subwf       scratch,W
            btfss       STATUS,2
            goto        skip13
            movf        tempbuf2,W
            subwf       max2,W
skip13
            btfss       STATUS,0
            goto        skip14
            goto        skip15
skip14
            movf        tempbuf2,W
            movwf       max2
            movf        (tempbuf2 + 1),W
            movwf       (max2 + 1)
skip15

;
;   Calculate minimum pipe temperature
;
;           .line   33; "calcDeltas.c"      if ( tempbuf2 < min2 )  min2 = tempbuf2;
            movf    (tempbuf2 + 1),W
            addlw   0x80
            movwf   scratch
            movf    (min2 + 1),W
            addlw   0x80
            subwf   scratch,W
            btfss   STATUS,2
            goto    skip16
            movf    min2,W
            subwf   tempbuf2,W
skip16
            btfss   STATUS,0
            goto    skip17
            goto    skip18
skip17
            movf    tempbuf2,W
            movwf   min2
            movf    (tempbuf2 + 1),W
            movwf   (min2 + 1)
skip18

 ;       .line   56; "TestDSmath.c"      return 0;
         movlw   0x00

         return
         end
