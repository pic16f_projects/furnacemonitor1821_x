;
;   getTemp2
;

            include     Processor.inc
            include     DS1821.inc
            radix       DEC

            global      getTemp2
		global		MasterReset2
			global	SendByte2
			global	PollStatus2
			global	ReadByte2
            global      init18212
            extern      Delay1ms

			extern	Delay100us,Delay10us
            extern      Delay1ms
;
			extern	tempbuf2

            ;extern      tempmin,tempmax
            extern      sign2
            extern      countrem
            extern      countdeg
            extern      min1,min2
            extern      max1,max2
            extern      mindelta,maxdelta


IODAT		udata_ovr
mask		res		1		; Mask to be ORed into result
count		res		1		; Loop counter

SHSP		udata_ovr
bi			res		1		; 9 if 9 bits to be read
buf			res		2		; Storage of intermediate result

SHSP		udata_ovr
by			res		1		; Storage for byte to be sent


IO          code

getTemp2:
            call        MasterReset2
			;                       SendByte2(STARTCONV);
            movlw       STARTCONV
            call        SendByte2
			;
			;                       //Hier Status Polling f�r Ende der Messung
			;                       PollStatus2();
            call        PollStatus2
			;
			;                       MasterReset2();
            call        MasterReset2
			;                       SendByte2(READTEMP);
            movlw       READTEMP
            call        SendByte2
			;                       ReadByte2(8);       //Temperatur lesen
            movlw       .8
            call        ReadByte2
			;                       sign2=(int)(tempbuf2&0xFF); //Nur die unteren 8 Bits nehmen !
            movf        tempbuf2,W
            movwf       sign2
			;
			;                       //Die beiden 9 Bit Register lesen
			;                       MasterReset2();
            call        MasterReset2
			;                       SendByte2(READCOUNT);
            movlw       READCOUNT
            call        SendByte2
			;                       ReadByte2(9);       //Remain lesen
            movlw       .9
            call        ReadByte2
			;                       countrem=tempbuf2;
            movf        tempbuf2,W
            movwf       countrem
            movf        tempbuf2+1,W
            movwf       countrem+1
			;
			;                       MasterReset2();
            call        MasterReset2
			;                       SendByte2(LOADREM); //Zweites Register laden Load second register
            movlw       LOADCOUNT
            call        SendByte2
			;
			;                       MasterReset2();
            call        MasterReset2
			;                       SendByte2(READCOUNT);
            movlw       READCOUNT
            call        SendByte2
			;                       ReadByte2(9);       //Degree lesen Read Degree
            movlw       .9
            call        ReadByte2
			;                       countdeg=tempbuf2;
            movf        tempbuf2,W
            movwf       countdeg
            movf        tempbuf2+1,W
            movwf       countdeg+1

            return


		title		'MasterReset2 - Reset the DS1821'
		subtitle	'Part of 1821PICel'
		list		b=4,c=132,n=85,x=Off

		;// Send the master reset sequence and query
		;// for presence of sensor

;	$Revision: 0.7 $  $Date: 2007/05/30 13:46:30 $


MasterReset2
    ;
    ; DANGER WILL ROBINSON -- this isn't the safest code
    ; in the world.  Because of RMW issues this could cause
    ; real consternation.  Works OK on the PIC-EL, but
    ; be careful if you have other stuff on PORTA
    ;

        ; Pulse the data pin for 600 us
		banksel		PORTA
		bcf   		DQ1

		movlw		.6
		call		Delay100us

		bsf			DQ1

		call		Delay10us

		; Wait for the pin to be driven low
m014
		btfsc		DQ1
		goto		m014

		; and then high
m015
		btfss		DQ1
		goto		m015

		; And wait another 600us
m016
		movlw		.6
		goto		Delay100us

			title		'PollStatus2 - Reset the 1821 and wait until ready'
			subtitle	'Part of 1821PICelR'
			list		b=4,c=132,n=85,x=Off

;	$Revision: 0.6 $  $Date: 2007/05/30 14:22:32 $



PollStatus2

			; Reset the DS1821, then loop reading status until
			; the high bit (DONE) of the returned status is set.

m012
			call	MasterReset2

			; Send the command to read the status
			movlw	READSTATUS
			call	SendByte2
			; Get the 8-bit result
            ;   Note this little bit of weirdness that ReadByte2
            ;   will read 8 or 9 bits depending on what is in W
			movlw	.8
			call	ReadByte2
			; Keep doing this until the DSDONE bit is set
			btfss	tempbuf2,DSDONE
			goto	m012
			return



		title		'ReadByte2 - Read a byte from the DS1821'
		subtitle	'Part of 1821PICelR'
		list		b=4,c=132,n=85,x=Off

;	Reads a byte from the DS1821 - result is stored in tempbuf2.  If
;	the W contains a 9 on entry, 9 bits are actually read.  The
;	buffer 'tempbuf2' is two bytes long.

			;void ReadByte2(unsigned char bi)

;	$Revision: 0.10 $  $Date: 2007/05/30 12:49:58 $


				;void ReadByte2(unsigned char bi)
				;{
ReadByte2
			movwf	bi
			; mask=0x01;
			movlw	B'00000001'
			movwf	mask
			; buf=0;
			clrf	buf

			; The variable mask will start out as a 1, then it is
			; shifted left each time through the loop.  mask is ORed
			; into the result depending on whether the DS1821 returns
			; a 0 or 1 for the corresponding bit.

			; for(count=0; count<8; count++) //bi Bits empfangen (receive bi bits)
			clrf	count
m021
			movlw	.8
			subwf	count,W
			btfsc	STATUS,C
			goto	m024
			;   DQ1=0; //
			banksel	PORTA	; Left inside loop to preserve timing
			bcf		DQ1
			nop
			nop
			;   DQ1=1; //Leitung freigeben (Release line)
			bsf		DQ1
			nop
			nop
			nop
			nop
			nop
			nop
			nop
			nop

			;   if(DQ1==1) buf|=mask;
			btfss	DQ1
			goto	m022
			movf	mask,W
			iorwf	buf,F
			;   else while(DQ1==0);  //Lange 0-Phase vom DS abwarten (Long wait 0-phase of the DS)
			goto	m023
m022
			btfsc	DQ1
			goto	m023
			goto	m022
			;   Delay100us(1);
m023
			movlw	.1
			call	Delay100us

			;   mask<<=1; //N�chstes Bit (Next bit)
			bcf		STATUS,C
			rlf		mask,F
			;  }
			incf	count,F
			goto	m021

m024
			; tempbuf2=buf;
			; So far the high 8 bits must be clear
			;
			; The point of this is not entirely clear to me (WB8RCR).
			; Perhaps on some processor/compiler where access to stack
			; variables is faster than global maybe, but this isn't the
			; case on the PIC.  Why not build the result directly
			; in tempbuf2?
			movf	buf,W
			movwf	tempbuf2
			clrf	tempbuf2+1

			; if(bi==9)
			;
			; If we came in with a 9 in W, then get one more bit
			; This bit will be saved in the high byte of tempbuf2
			movf	bi,W
			xorlw	.9
			btfss	STATUS,Z
			goto	m026

			;   DQ1=0; //Flanke f�r DS (Edge for DS)
			bcf		DQ1
			nop
			nop
			;   DQ1=1; //Leitung freigeben (Release line)
			bsf		DQ1
			nop
			nop
			nop
			nop
			nop
			nop
			nop
			nop
			;   if(DQ1==1) tempbuf2+=0x100; //9tes Bit lesen (Read 9th bit)
			btfss	DQ1
			goto	m025
			incf	tempbuf2+1,F
			;   else while(DQ1==0);  //Lange 0-Phase vom DS abwarten (Long wait 0-phase of the DS)
			goto	m026
m025
			btfss	DQ1
			goto	m025

m026
			; Delay100us(6);
			movlw	.6
			goto	Delay100us



		title		'SendByte2 - Send a byte to the DS1821'
		subtitle	'Part of 1821PICelR'
		list		b=4,c=132,n=85,x=Off
;
;	Sends a byte to the DS1821.  Enter with the byte to be
;	sent in W.

			;void SendByte2(unsigned char by)

;	$Revision: 0.4 $  $Date: 2005/11/20 14:52:00 $



			;void SendByte2(unsigned char by)
SendByte2
			; Save off the argument in W
			movwf	by
			; Initialize the bit mask
			; mask=0x01;
			movlw	B'00000001'
			movwf	mask

			; Loop once for each bit in the datum.  Each time the mask
			; is shifted and XORed with the ddatum to see whether the
			; current bit it a 0 or 1.  Then the appropriate signal is
			; sent to the DS1821 for the bit.

			; for(count=0; count<8; count++) //8Bits senden
			clrf	count	; Initialize loop counter
m017
			; Check whether loop is done
			movlw	.8
			subwf	count,W
			btfsc	STATUS,C
			goto	m020

			;   if((mask&by)==0) //Ein 0-Bit
			movf	by,W	; Pick up the data byte
			andwf	mask,W	; And pick off the current bit
			xorlw	.0		; Test whether a zero or one bit
			btfss	STATUS,Z
			goto	m018

			; Its a zero bit
			;     DQ1=0; //Now wait min 60us
			banksel	PORTA
			bcf		DQ1
			;     Delay10us(); 9 times
			call	Delay10us
			call	Delay10us
			call	Delay10us
			call	Delay10us
			call	Delay10us
			call	Delay10us
			call	Delay10us
			call	Delay10us
			call	Delay10us

			goto	m019
m018
			; Its a one bit
			;   else             //Ein 1-Bit
			;    {
			;     DQ1=0;
			bcf		DQ1
			;      nop         //Wait briefly
			nop
			nop

			;   DQ1=1;
m019
			bsf		DQ1
			;   Delay100us(1);
			movlw	.1
			call	Delay100us
			;   mask<<=1; //Next Bit - shift mask left
			bcf		STATUS,C
			rlf		mask,F

			incf	count,F		; Next bit
			goto	m017
m020
			return


;

init18212:
			;                       MasterReset();
            call        MasterReset2
			;                       SendByte(WRITESTATUS);
            movlw       WRITESTATUS
            call        SendByte2
			;                       SendByte(0x41); //One Shot Operation einstellen
            movlw       DSCONFIG | DS1SHOT
            call        SendByte2
			;                       Delay1ms(255);  //Etwas warten
            movlw       .255
            call        Delay1ms
			;

 ;
 ;  Initialization min, max
 ;
 ;          .line       37; "TestDSmath.c"      min1 = 12500;
            movlw       0xd4
            banksel     min1
            movwf       min1
            movlw       0x30
            movwf       min1+1
 ;          .line       38; "TestDSmath.c"      min2 = 12500;
            movlw       0xd4
            movwf       min2
            movlw       0x30
            movwf       min2+1
 ;          .line       39; "TestDSmath.c"      mindelta = 12500;
            movlw       0xd4
            movwf       mindelta
            movlw       0x30
            movwf       mindelta+1
 ;          .line       41; "TestDSmath.c"      max1 = -5500;
            movlw       0x84
            movwf       max1
            movlw       0xea
            movwf       max1+1
 ;          .line       42; "TestDSmath.c"      max2 = -5500;
            movlw       0x84
            movwf       max2
            movlw       0xea
            movwf       max2+1
 ;          .line       43; "TestDSmath.c"      maxdelta = -5500;
            movlw       0x84
            movwf       maxdelta
            movlw       0xea
            movwf       maxdelta+1


            return
            end


