;	DS1821.inc - Definitions for DS1821 I/O
;	$Revision: 0.6 $  $Date: 2005/11/20 16:43:32 $
			nolist

;	Pin for communication with DS1821
#define		DQ		PORTA,4
#define     DQ1     PORTA,3

; DS1821 Commands
READSTATUS	equ		H'ac'
WRITESTATUS	equ		H'0c'
STARTCONV	equ		H'ee'
READTEMP	equ		H'aa'
READCOUNT	equ		H'a0'
LOADCOUNT	equ		H'41'

; DSS1821 Status Register
DSDONE		equ		7		; Bit Number
DS1SHOT		equ		H'01'	; Mask
DSCONFIG	equ		H'40'	; Always on to write config
			list

