		title		'ReadByte - Read a byte from the DS1821'
		subtitle	'Part of 1821PICelR'
		list		b=4,c=132,n=85,x=Off

;	Reads a byte from the DS1821 - result is stored in tempbuf.  If
;	the W contains a 9 on entry, 9 bits are actually read.  The
;	buffer 'tempbuf' is two bytes long.

			;void ReadByte(unsigned char bi)

;	$Revision: 0.10 $  $Date: 2007/05/30 12:49:58 $

			include	Processor.inc
			include	DS1821.inc
			global	ReadByte
			extern	Delay100us,Delay10us
			extern	tempbuf

IODAT		udata_ovr
mask		res		1		; Mask to be ORed into result
count		res		1		; Loop counter

SHSP		udata_ovr
bi			res		1		; 9 if 9 bits to be read
buf			res		2		; Storage of intermediate result
	
IO			code

				;void ReadByte(unsigned char bi)
				;{
ReadByte
			movwf	bi
			; mask=0x01;
			movlw	B'00000001'
			movwf	mask
			; buf=0;
			clrf	buf

			; The variable mask will start out as a 1, then it is
			; shifted left each time through the loop.  mask is ORed
			; into the result depending on whether the DS1821 returns
			; a 0 or 1 for the corresponding bit.

			; for(count=0; count<8; count++) //bi Bits empfangen (receive bi bits)
			clrf	count
m021
			movlw	.8
			subwf	count,W
			btfsc	STATUS,C
			goto	m024
			;   DQ=0; //
			banksel	PORTA	; Left inside loop to preserve timing
			bcf		DQ
			nop
			nop
			;   DQ=1; //Leitung freigeben (Release line)
			bsf		DQ
			nop
			nop
			nop
			nop
			nop
			nop
			nop
			nop

			;   if(DQ==1) buf|=mask;
			btfss	DQ
			goto	m022
			movf	mask,W
			iorwf	buf,F
			;   else while(DQ==0);  //Lange 0-Phase vom DS abwarten (Long wait 0-phase of the DS)
			goto	m023
m022
			btfsc	DQ
			goto	m023
			goto	m022
			;   Delay100us(1);
m023
			movlw	.1
			call	Delay100us

			;   mask<<=1; //N�chstes Bit (Next bit)
			bcf		STATUS,C
			rlf		mask,F
			;  }
			incf	count,F
			goto	m021

m024
			; tempbuf=buf;
			; So far the high 8 bits must be clear
			;
			; The point of this is not entirely clear to me (WB8RCR).
			; Perhaps on some processor/compiler where access to stack
			; variables is faster than global maybe, but this isn't the
			; case on the PIC.  Why not build the result directly
			; in tempbuf?
			movf	buf,W
			movwf	tempbuf
			clrf	tempbuf+1

			; if(bi==9)
			;
			; If we came in with a 9 in W, then get one more bit
			; This bit will be saved in the high byte of tempbuf
			movf	bi,W
			xorlw	.9
			btfss	STATUS,Z
			goto	m026

			;   DQ=0; //Flanke f�r DS (Edge for DS)
			bcf		DQ
			nop
			nop
			;   DQ=1; //Leitung freigeben (Release line)
			bsf		DQ
			nop
			nop
			nop
			nop
			nop
			nop
			nop
			nop
			;   if(DQ==1) tempbuf+=0x100; //9tes Bit lesen (Read 9th bit)
			btfss	DQ	
			goto	m025
			incf	tempbuf+1,F
			;   else while(DQ==0);  //Lange 0-Phase vom DS abwarten (Long wait 0-phase of the DS)
			goto	m026
m025
			btfss	DQ
			goto	m025

m026
			; Delay100us(6);
			movlw	.6
			goto	Delay100us

			end
