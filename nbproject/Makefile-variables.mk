#
# Generated - do not edit!
#
# NOCDDL
#
CND_BASEDIR=`pwd`
# Blue configuration
CND_ARTIFACT_DIR_Blue=dist/Blue/production
CND_ARTIFACT_NAME_Blue=FurnaceMonitor1821.X.production.hex
CND_ARTIFACT_PATH_Blue=dist/Blue/production/FurnaceMonitor1821.X.production.hex
CND_PACKAGE_DIR_Blue=${CND_DISTDIR}/Blue/package
CND_PACKAGE_NAME_Blue=furnacemonitor1821.x.tar
CND_PACKAGE_PATH_Blue=${CND_DISTDIR}/Blue/package/furnacemonitor1821.x.tar
# Red configuration
CND_ARTIFACT_DIR_Red=dist/Red/production
CND_ARTIFACT_NAME_Red=FurnaceMonitor1821.X.production.hex
CND_ARTIFACT_PATH_Red=dist/Red/production/FurnaceMonitor1821.X.production.hex
CND_PACKAGE_DIR_Red=${CND_DISTDIR}/Red/package
CND_PACKAGE_NAME_Red=furnacemonitor1821.x.tar
CND_PACKAGE_PATH_Red=${CND_DISTDIR}/Red/package/furnacemonitor1821.x.tar
# Simulate configuration
CND_ARTIFACT_DIR_Simulate=dist/Simulate/production
CND_ARTIFACT_NAME_Simulate=FurnaceMonitor1821.X.production.hex
CND_ARTIFACT_PATH_Simulate=dist/Simulate/production/FurnaceMonitor1821.X.production.hex
CND_PACKAGE_DIR_Simulate=${CND_DISTDIR}/Simulate/package
CND_PACKAGE_NAME_Simulate=furnacemonitor1821.x.tar
CND_PACKAGE_PATH_Simulate=${CND_DISTDIR}/Simulate/package/furnacemonitor1821.x.tar
