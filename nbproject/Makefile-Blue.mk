#
# Generated Makefile - do not edit!
#
# Edit the Makefile in the project folder instead (../Makefile). Each target
# has a -pre and a -post target defined where you can add customized code.
#
# This makefile implements configuration specific macros and targets.


# Include project Makefile
ifeq "${IGNORE_LOCAL}" "TRUE"
# do not include local makefile. User is passing all local related variables already
else
include Makefile
# Include makefile containing local settings
ifeq "$(wildcard nbproject/Makefile-local-Blue.mk)" "nbproject/Makefile-local-Blue.mk"
include nbproject/Makefile-local-Blue.mk
endif
endif

# Environment
MKDIR=mkdir -p
RM=rm -f 
MV=mv 
CP=cp 

# Macros
CND_CONF=Blue
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
IMAGE_TYPE=debug
OUTPUT_SUFFIX=cof
DEBUGGABLE_SUFFIX=cof
FINAL_IMAGE=dist/${CND_CONF}/${IMAGE_TYPE}/FurnaceMonitor1821.X.${IMAGE_TYPE}.${OUTPUT_SUFFIX}
else
IMAGE_TYPE=production
OUTPUT_SUFFIX=hex
DEBUGGABLE_SUFFIX=cof
FINAL_IMAGE=dist/${CND_CONF}/${IMAGE_TYPE}/FurnaceMonitor1821.X.${IMAGE_TYPE}.${OUTPUT_SUFFIX}
endif

# Object Directory
OBJECTDIR=build/${CND_CONF}/${IMAGE_TYPE}

# Distribution Directory
DISTDIR=dist/${CND_CONF}/${IMAGE_TYPE}

# Source Files Quoted if spaced
SOURCEFILES_QUOTED_IF_SPACED=LCD.asm _divS16.asm binBCD16.asm del100us.asm del10us.asm del1ms.asm masterReset.asm pollStatus.asm readBYTE.asm sendByte.asm showBCD.asm del1ms32.asm init1821.asm getTemp.asm SecondDS.asm convert1.asm convert2.asm calcDeltas.asm showExtremes.asm FurnaceMonitor1821.asm

# Object Files Quoted if spaced
OBJECTFILES_QUOTED_IF_SPACED=${OBJECTDIR}/LCD.o ${OBJECTDIR}/_divS16.o ${OBJECTDIR}/binBCD16.o ${OBJECTDIR}/del100us.o ${OBJECTDIR}/del10us.o ${OBJECTDIR}/del1ms.o ${OBJECTDIR}/masterReset.o ${OBJECTDIR}/pollStatus.o ${OBJECTDIR}/readBYTE.o ${OBJECTDIR}/sendByte.o ${OBJECTDIR}/showBCD.o ${OBJECTDIR}/del1ms32.o ${OBJECTDIR}/init1821.o ${OBJECTDIR}/getTemp.o ${OBJECTDIR}/SecondDS.o ${OBJECTDIR}/convert1.o ${OBJECTDIR}/convert2.o ${OBJECTDIR}/calcDeltas.o ${OBJECTDIR}/showExtremes.o ${OBJECTDIR}/FurnaceMonitor1821.o
POSSIBLE_DEPFILES=${OBJECTDIR}/LCD.o.d ${OBJECTDIR}/_divS16.o.d ${OBJECTDIR}/binBCD16.o.d ${OBJECTDIR}/del100us.o.d ${OBJECTDIR}/del10us.o.d ${OBJECTDIR}/del1ms.o.d ${OBJECTDIR}/masterReset.o.d ${OBJECTDIR}/pollStatus.o.d ${OBJECTDIR}/readBYTE.o.d ${OBJECTDIR}/sendByte.o.d ${OBJECTDIR}/showBCD.o.d ${OBJECTDIR}/del1ms32.o.d ${OBJECTDIR}/init1821.o.d ${OBJECTDIR}/getTemp.o.d ${OBJECTDIR}/SecondDS.o.d ${OBJECTDIR}/convert1.o.d ${OBJECTDIR}/convert2.o.d ${OBJECTDIR}/calcDeltas.o.d ${OBJECTDIR}/showExtremes.o.d ${OBJECTDIR}/FurnaceMonitor1821.o.d

# Object Files
OBJECTFILES=${OBJECTDIR}/LCD.o ${OBJECTDIR}/_divS16.o ${OBJECTDIR}/binBCD16.o ${OBJECTDIR}/del100us.o ${OBJECTDIR}/del10us.o ${OBJECTDIR}/del1ms.o ${OBJECTDIR}/masterReset.o ${OBJECTDIR}/pollStatus.o ${OBJECTDIR}/readBYTE.o ${OBJECTDIR}/sendByte.o ${OBJECTDIR}/showBCD.o ${OBJECTDIR}/del1ms32.o ${OBJECTDIR}/init1821.o ${OBJECTDIR}/getTemp.o ${OBJECTDIR}/SecondDS.o ${OBJECTDIR}/convert1.o ${OBJECTDIR}/convert2.o ${OBJECTDIR}/calcDeltas.o ${OBJECTDIR}/showExtremes.o ${OBJECTDIR}/FurnaceMonitor1821.o

# Source Files
SOURCEFILES=LCD.asm _divS16.asm binBCD16.asm del100us.asm del10us.asm del1ms.asm masterReset.asm pollStatus.asm readBYTE.asm sendByte.asm showBCD.asm del1ms32.asm init1821.asm getTemp.asm SecondDS.asm convert1.asm convert2.asm calcDeltas.asm showExtremes.asm FurnaceMonitor1821.asm


CFLAGS=
ASFLAGS=
LDLIBSOPTIONS=

############# Tool locations ##########################################
# If you copy a project from one host to another, the path where the  #
# compiler is installed may be different.                             #
# If you open this project with MPLAB X in the new host, this         #
# makefile will be regenerated and the paths will be corrected.       #
#######################################################################
# fixDeps replaces a bunch of sed/cat/printf statements that slow down the build
FIXDEPS=fixDeps

.build-conf:  ${BUILD_SUBPROJECTS}
	${MAKE}  -f nbproject/Makefile-Blue.mk dist/${CND_CONF}/${IMAGE_TYPE}/FurnaceMonitor1821.X.${IMAGE_TYPE}.${OUTPUT_SUFFIX}

MP_PROCESSOR_OPTION=16f88
MP_LINKER_DEBUG_OPTION=-r=ROM@0xF00:0xFFF -r=RAM@SHARE:0x70:0x70 -r=RAM@SHARE:0xF0:0xF0 -r=RAM@SHARE:0x170:0x170 -r=RAM@GPR:0x1E7:0x1EF -r=RAM@SHARE:0x1F0:0x1F0
# ------------------------------------------------------------------------------------
# Rules for buildStep: assemble
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
${OBJECTDIR}/LCD.o: LCD.asm  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} ${OBJECTDIR} 
	@${RM} ${OBJECTDIR}/LCD.o.d 
	@${RM} ${OBJECTDIR}/LCD.o 
	@${FIXDEPS} dummy.d -e "${OBJECTDIR}/LCD.err" $(SILENT) -c ${MP_AS} $(MP_EXTRA_AS_PRE) -d__DEBUG -d__MPLAB_DEBUGGER_ICD3=1 -q -p$(MP_PROCESSOR_OPTION) -u  -l\\\"${OBJECTDIR}/LCD.lst\\\" -e\\\"${OBJECTDIR}/LCD.err\\\" $(ASM_OPTIONS)   -o\\\"${OBJECTDIR}/LCD.o\\\" \\\"LCD.asm\\\" 
	@${DEP_GEN} -d "${OBJECTDIR}/LCD.o"
	@${FIXDEPS} "${OBJECTDIR}/LCD.o.d" $(SILENT) -rsi ${MP_AS_DIR} -c18 
	
${OBJECTDIR}/_divS16.o: _divS16.asm  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} ${OBJECTDIR} 
	@${RM} ${OBJECTDIR}/_divS16.o.d 
	@${RM} ${OBJECTDIR}/_divS16.o 
	@${FIXDEPS} dummy.d -e "${OBJECTDIR}/_divS16.err" $(SILENT) -c ${MP_AS} $(MP_EXTRA_AS_PRE) -d__DEBUG -d__MPLAB_DEBUGGER_ICD3=1 -q -p$(MP_PROCESSOR_OPTION) -u  -l\\\"${OBJECTDIR}/_divS16.lst\\\" -e\\\"${OBJECTDIR}/_divS16.err\\\" $(ASM_OPTIONS)   -o\\\"${OBJECTDIR}/_divS16.o\\\" \\\"_divS16.asm\\\" 
	@${DEP_GEN} -d "${OBJECTDIR}/_divS16.o"
	@${FIXDEPS} "${OBJECTDIR}/_divS16.o.d" $(SILENT) -rsi ${MP_AS_DIR} -c18 
	
${OBJECTDIR}/binBCD16.o: binBCD16.asm  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} ${OBJECTDIR} 
	@${RM} ${OBJECTDIR}/binBCD16.o.d 
	@${RM} ${OBJECTDIR}/binBCD16.o 
	@${FIXDEPS} dummy.d -e "${OBJECTDIR}/binBCD16.err" $(SILENT) -c ${MP_AS} $(MP_EXTRA_AS_PRE) -d__DEBUG -d__MPLAB_DEBUGGER_ICD3=1 -q -p$(MP_PROCESSOR_OPTION) -u  -l\\\"${OBJECTDIR}/binBCD16.lst\\\" -e\\\"${OBJECTDIR}/binBCD16.err\\\" $(ASM_OPTIONS)   -o\\\"${OBJECTDIR}/binBCD16.o\\\" \\\"binBCD16.asm\\\" 
	@${DEP_GEN} -d "${OBJECTDIR}/binBCD16.o"
	@${FIXDEPS} "${OBJECTDIR}/binBCD16.o.d" $(SILENT) -rsi ${MP_AS_DIR} -c18 
	
${OBJECTDIR}/del100us.o: del100us.asm  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} ${OBJECTDIR} 
	@${RM} ${OBJECTDIR}/del100us.o.d 
	@${RM} ${OBJECTDIR}/del100us.o 
	@${FIXDEPS} dummy.d -e "${OBJECTDIR}/del100us.err" $(SILENT) -c ${MP_AS} $(MP_EXTRA_AS_PRE) -d__DEBUG -d__MPLAB_DEBUGGER_ICD3=1 -q -p$(MP_PROCESSOR_OPTION) -u  -l\\\"${OBJECTDIR}/del100us.lst\\\" -e\\\"${OBJECTDIR}/del100us.err\\\" $(ASM_OPTIONS)   -o\\\"${OBJECTDIR}/del100us.o\\\" \\\"del100us.asm\\\" 
	@${DEP_GEN} -d "${OBJECTDIR}/del100us.o"
	@${FIXDEPS} "${OBJECTDIR}/del100us.o.d" $(SILENT) -rsi ${MP_AS_DIR} -c18 
	
${OBJECTDIR}/del10us.o: del10us.asm  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} ${OBJECTDIR} 
	@${RM} ${OBJECTDIR}/del10us.o.d 
	@${RM} ${OBJECTDIR}/del10us.o 
	@${FIXDEPS} dummy.d -e "${OBJECTDIR}/del10us.err" $(SILENT) -c ${MP_AS} $(MP_EXTRA_AS_PRE) -d__DEBUG -d__MPLAB_DEBUGGER_ICD3=1 -q -p$(MP_PROCESSOR_OPTION) -u  -l\\\"${OBJECTDIR}/del10us.lst\\\" -e\\\"${OBJECTDIR}/del10us.err\\\" $(ASM_OPTIONS)   -o\\\"${OBJECTDIR}/del10us.o\\\" \\\"del10us.asm\\\" 
	@${DEP_GEN} -d "${OBJECTDIR}/del10us.o"
	@${FIXDEPS} "${OBJECTDIR}/del10us.o.d" $(SILENT) -rsi ${MP_AS_DIR} -c18 
	
${OBJECTDIR}/del1ms.o: del1ms.asm  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} ${OBJECTDIR} 
	@${RM} ${OBJECTDIR}/del1ms.o.d 
	@${RM} ${OBJECTDIR}/del1ms.o 
	@${FIXDEPS} dummy.d -e "${OBJECTDIR}/del1ms.err" $(SILENT) -c ${MP_AS} $(MP_EXTRA_AS_PRE) -d__DEBUG -d__MPLAB_DEBUGGER_ICD3=1 -q -p$(MP_PROCESSOR_OPTION) -u  -l\\\"${OBJECTDIR}/del1ms.lst\\\" -e\\\"${OBJECTDIR}/del1ms.err\\\" $(ASM_OPTIONS)   -o\\\"${OBJECTDIR}/del1ms.o\\\" \\\"del1ms.asm\\\" 
	@${DEP_GEN} -d "${OBJECTDIR}/del1ms.o"
	@${FIXDEPS} "${OBJECTDIR}/del1ms.o.d" $(SILENT) -rsi ${MP_AS_DIR} -c18 
	
${OBJECTDIR}/masterReset.o: masterReset.asm  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} ${OBJECTDIR} 
	@${RM} ${OBJECTDIR}/masterReset.o.d 
	@${RM} ${OBJECTDIR}/masterReset.o 
	@${FIXDEPS} dummy.d -e "${OBJECTDIR}/masterReset.err" $(SILENT) -c ${MP_AS} $(MP_EXTRA_AS_PRE) -d__DEBUG -d__MPLAB_DEBUGGER_ICD3=1 -q -p$(MP_PROCESSOR_OPTION) -u  -l\\\"${OBJECTDIR}/masterReset.lst\\\" -e\\\"${OBJECTDIR}/masterReset.err\\\" $(ASM_OPTIONS)   -o\\\"${OBJECTDIR}/masterReset.o\\\" \\\"masterReset.asm\\\" 
	@${DEP_GEN} -d "${OBJECTDIR}/masterReset.o"
	@${FIXDEPS} "${OBJECTDIR}/masterReset.o.d" $(SILENT) -rsi ${MP_AS_DIR} -c18 
	
${OBJECTDIR}/pollStatus.o: pollStatus.asm  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} ${OBJECTDIR} 
	@${RM} ${OBJECTDIR}/pollStatus.o.d 
	@${RM} ${OBJECTDIR}/pollStatus.o 
	@${FIXDEPS} dummy.d -e "${OBJECTDIR}/pollStatus.err" $(SILENT) -c ${MP_AS} $(MP_EXTRA_AS_PRE) -d__DEBUG -d__MPLAB_DEBUGGER_ICD3=1 -q -p$(MP_PROCESSOR_OPTION) -u  -l\\\"${OBJECTDIR}/pollStatus.lst\\\" -e\\\"${OBJECTDIR}/pollStatus.err\\\" $(ASM_OPTIONS)   -o\\\"${OBJECTDIR}/pollStatus.o\\\" \\\"pollStatus.asm\\\" 
	@${DEP_GEN} -d "${OBJECTDIR}/pollStatus.o"
	@${FIXDEPS} "${OBJECTDIR}/pollStatus.o.d" $(SILENT) -rsi ${MP_AS_DIR} -c18 
	
${OBJECTDIR}/readBYTE.o: readBYTE.asm  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} ${OBJECTDIR} 
	@${RM} ${OBJECTDIR}/readBYTE.o.d 
	@${RM} ${OBJECTDIR}/readBYTE.o 
	@${FIXDEPS} dummy.d -e "${OBJECTDIR}/readBYTE.err" $(SILENT) -c ${MP_AS} $(MP_EXTRA_AS_PRE) -d__DEBUG -d__MPLAB_DEBUGGER_ICD3=1 -q -p$(MP_PROCESSOR_OPTION) -u  -l\\\"${OBJECTDIR}/readBYTE.lst\\\" -e\\\"${OBJECTDIR}/readBYTE.err\\\" $(ASM_OPTIONS)   -o\\\"${OBJECTDIR}/readBYTE.o\\\" \\\"readBYTE.asm\\\" 
	@${DEP_GEN} -d "${OBJECTDIR}/readBYTE.o"
	@${FIXDEPS} "${OBJECTDIR}/readBYTE.o.d" $(SILENT) -rsi ${MP_AS_DIR} -c18 
	
${OBJECTDIR}/sendByte.o: sendByte.asm  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} ${OBJECTDIR} 
	@${RM} ${OBJECTDIR}/sendByte.o.d 
	@${RM} ${OBJECTDIR}/sendByte.o 
	@${FIXDEPS} dummy.d -e "${OBJECTDIR}/sendByte.err" $(SILENT) -c ${MP_AS} $(MP_EXTRA_AS_PRE) -d__DEBUG -d__MPLAB_DEBUGGER_ICD3=1 -q -p$(MP_PROCESSOR_OPTION) -u  -l\\\"${OBJECTDIR}/sendByte.lst\\\" -e\\\"${OBJECTDIR}/sendByte.err\\\" $(ASM_OPTIONS)   -o\\\"${OBJECTDIR}/sendByte.o\\\" \\\"sendByte.asm\\\" 
	@${DEP_GEN} -d "${OBJECTDIR}/sendByte.o"
	@${FIXDEPS} "${OBJECTDIR}/sendByte.o.d" $(SILENT) -rsi ${MP_AS_DIR} -c18 
	
${OBJECTDIR}/showBCD.o: showBCD.asm  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} ${OBJECTDIR} 
	@${RM} ${OBJECTDIR}/showBCD.o.d 
	@${RM} ${OBJECTDIR}/showBCD.o 
	@${FIXDEPS} dummy.d -e "${OBJECTDIR}/showBCD.err" $(SILENT) -c ${MP_AS} $(MP_EXTRA_AS_PRE) -d__DEBUG -d__MPLAB_DEBUGGER_ICD3=1 -q -p$(MP_PROCESSOR_OPTION) -u  -l\\\"${OBJECTDIR}/showBCD.lst\\\" -e\\\"${OBJECTDIR}/showBCD.err\\\" $(ASM_OPTIONS)   -o\\\"${OBJECTDIR}/showBCD.o\\\" \\\"showBCD.asm\\\" 
	@${DEP_GEN} -d "${OBJECTDIR}/showBCD.o"
	@${FIXDEPS} "${OBJECTDIR}/showBCD.o.d" $(SILENT) -rsi ${MP_AS_DIR} -c18 
	
${OBJECTDIR}/del1ms32.o: del1ms32.asm  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} ${OBJECTDIR} 
	@${RM} ${OBJECTDIR}/del1ms32.o.d 
	@${RM} ${OBJECTDIR}/del1ms32.o 
	@${FIXDEPS} dummy.d -e "${OBJECTDIR}/del1ms32.err" $(SILENT) -c ${MP_AS} $(MP_EXTRA_AS_PRE) -d__DEBUG -d__MPLAB_DEBUGGER_ICD3=1 -q -p$(MP_PROCESSOR_OPTION) -u  -l\\\"${OBJECTDIR}/del1ms32.lst\\\" -e\\\"${OBJECTDIR}/del1ms32.err\\\" $(ASM_OPTIONS)   -o\\\"${OBJECTDIR}/del1ms32.o\\\" \\\"del1ms32.asm\\\" 
	@${DEP_GEN} -d "${OBJECTDIR}/del1ms32.o"
	@${FIXDEPS} "${OBJECTDIR}/del1ms32.o.d" $(SILENT) -rsi ${MP_AS_DIR} -c18 
	
${OBJECTDIR}/init1821.o: init1821.asm  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} ${OBJECTDIR} 
	@${RM} ${OBJECTDIR}/init1821.o.d 
	@${RM} ${OBJECTDIR}/init1821.o 
	@${FIXDEPS} dummy.d -e "${OBJECTDIR}/init1821.err" $(SILENT) -c ${MP_AS} $(MP_EXTRA_AS_PRE) -d__DEBUG -d__MPLAB_DEBUGGER_ICD3=1 -q -p$(MP_PROCESSOR_OPTION) -u  -l\\\"${OBJECTDIR}/init1821.lst\\\" -e\\\"${OBJECTDIR}/init1821.err\\\" $(ASM_OPTIONS)   -o\\\"${OBJECTDIR}/init1821.o\\\" \\\"init1821.asm\\\" 
	@${DEP_GEN} -d "${OBJECTDIR}/init1821.o"
	@${FIXDEPS} "${OBJECTDIR}/init1821.o.d" $(SILENT) -rsi ${MP_AS_DIR} -c18 
	
${OBJECTDIR}/getTemp.o: getTemp.asm  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} ${OBJECTDIR} 
	@${RM} ${OBJECTDIR}/getTemp.o.d 
	@${RM} ${OBJECTDIR}/getTemp.o 
	@${FIXDEPS} dummy.d -e "${OBJECTDIR}/getTemp.err" $(SILENT) -c ${MP_AS} $(MP_EXTRA_AS_PRE) -d__DEBUG -d__MPLAB_DEBUGGER_ICD3=1 -q -p$(MP_PROCESSOR_OPTION) -u  -l\\\"${OBJECTDIR}/getTemp.lst\\\" -e\\\"${OBJECTDIR}/getTemp.err\\\" $(ASM_OPTIONS)   -o\\\"${OBJECTDIR}/getTemp.o\\\" \\\"getTemp.asm\\\" 
	@${DEP_GEN} -d "${OBJECTDIR}/getTemp.o"
	@${FIXDEPS} "${OBJECTDIR}/getTemp.o.d" $(SILENT) -rsi ${MP_AS_DIR} -c18 
	
${OBJECTDIR}/SecondDS.o: SecondDS.asm  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} ${OBJECTDIR} 
	@${RM} ${OBJECTDIR}/SecondDS.o.d 
	@${RM} ${OBJECTDIR}/SecondDS.o 
	@${FIXDEPS} dummy.d -e "${OBJECTDIR}/SecondDS.err" $(SILENT) -c ${MP_AS} $(MP_EXTRA_AS_PRE) -d__DEBUG -d__MPLAB_DEBUGGER_ICD3=1 -q -p$(MP_PROCESSOR_OPTION) -u  -l\\\"${OBJECTDIR}/SecondDS.lst\\\" -e\\\"${OBJECTDIR}/SecondDS.err\\\" $(ASM_OPTIONS)   -o\\\"${OBJECTDIR}/SecondDS.o\\\" \\\"SecondDS.asm\\\" 
	@${DEP_GEN} -d "${OBJECTDIR}/SecondDS.o"
	@${FIXDEPS} "${OBJECTDIR}/SecondDS.o.d" $(SILENT) -rsi ${MP_AS_DIR} -c18 
	
${OBJECTDIR}/convert1.o: convert1.asm  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} ${OBJECTDIR} 
	@${RM} ${OBJECTDIR}/convert1.o.d 
	@${RM} ${OBJECTDIR}/convert1.o 
	@${FIXDEPS} dummy.d -e "${OBJECTDIR}/convert1.err" $(SILENT) -c ${MP_AS} $(MP_EXTRA_AS_PRE) -d__DEBUG -d__MPLAB_DEBUGGER_ICD3=1 -q -p$(MP_PROCESSOR_OPTION) -u  -l\\\"${OBJECTDIR}/convert1.lst\\\" -e\\\"${OBJECTDIR}/convert1.err\\\" $(ASM_OPTIONS)   -o\\\"${OBJECTDIR}/convert1.o\\\" \\\"convert1.asm\\\" 
	@${DEP_GEN} -d "${OBJECTDIR}/convert1.o"
	@${FIXDEPS} "${OBJECTDIR}/convert1.o.d" $(SILENT) -rsi ${MP_AS_DIR} -c18 
	
${OBJECTDIR}/convert2.o: convert2.asm  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} ${OBJECTDIR} 
	@${RM} ${OBJECTDIR}/convert2.o.d 
	@${RM} ${OBJECTDIR}/convert2.o 
	@${FIXDEPS} dummy.d -e "${OBJECTDIR}/convert2.err" $(SILENT) -c ${MP_AS} $(MP_EXTRA_AS_PRE) -d__DEBUG -d__MPLAB_DEBUGGER_ICD3=1 -q -p$(MP_PROCESSOR_OPTION) -u  -l\\\"${OBJECTDIR}/convert2.lst\\\" -e\\\"${OBJECTDIR}/convert2.err\\\" $(ASM_OPTIONS)   -o\\\"${OBJECTDIR}/convert2.o\\\" \\\"convert2.asm\\\" 
	@${DEP_GEN} -d "${OBJECTDIR}/convert2.o"
	@${FIXDEPS} "${OBJECTDIR}/convert2.o.d" $(SILENT) -rsi ${MP_AS_DIR} -c18 
	
${OBJECTDIR}/calcDeltas.o: calcDeltas.asm  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} ${OBJECTDIR} 
	@${RM} ${OBJECTDIR}/calcDeltas.o.d 
	@${RM} ${OBJECTDIR}/calcDeltas.o 
	@${FIXDEPS} dummy.d -e "${OBJECTDIR}/calcDeltas.err" $(SILENT) -c ${MP_AS} $(MP_EXTRA_AS_PRE) -d__DEBUG -d__MPLAB_DEBUGGER_ICD3=1 -q -p$(MP_PROCESSOR_OPTION) -u  -l\\\"${OBJECTDIR}/calcDeltas.lst\\\" -e\\\"${OBJECTDIR}/calcDeltas.err\\\" $(ASM_OPTIONS)   -o\\\"${OBJECTDIR}/calcDeltas.o\\\" \\\"calcDeltas.asm\\\" 
	@${DEP_GEN} -d "${OBJECTDIR}/calcDeltas.o"
	@${FIXDEPS} "${OBJECTDIR}/calcDeltas.o.d" $(SILENT) -rsi ${MP_AS_DIR} -c18 
	
${OBJECTDIR}/showExtremes.o: showExtremes.asm  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} ${OBJECTDIR} 
	@${RM} ${OBJECTDIR}/showExtremes.o.d 
	@${RM} ${OBJECTDIR}/showExtremes.o 
	@${FIXDEPS} dummy.d -e "${OBJECTDIR}/showExtremes.err" $(SILENT) -c ${MP_AS} $(MP_EXTRA_AS_PRE) -d__DEBUG -d__MPLAB_DEBUGGER_ICD3=1 -q -p$(MP_PROCESSOR_OPTION) -u  -l\\\"${OBJECTDIR}/showExtremes.lst\\\" -e\\\"${OBJECTDIR}/showExtremes.err\\\" $(ASM_OPTIONS)   -o\\\"${OBJECTDIR}/showExtremes.o\\\" \\\"showExtremes.asm\\\" 
	@${DEP_GEN} -d "${OBJECTDIR}/showExtremes.o"
	@${FIXDEPS} "${OBJECTDIR}/showExtremes.o.d" $(SILENT) -rsi ${MP_AS_DIR} -c18 
	
${OBJECTDIR}/FurnaceMonitor1821.o: FurnaceMonitor1821.asm  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} ${OBJECTDIR} 
	@${RM} ${OBJECTDIR}/FurnaceMonitor1821.o.d 
	@${RM} ${OBJECTDIR}/FurnaceMonitor1821.o 
	@${FIXDEPS} dummy.d -e "${OBJECTDIR}/FurnaceMonitor1821.err" $(SILENT) -c ${MP_AS} $(MP_EXTRA_AS_PRE) -d__DEBUG -d__MPLAB_DEBUGGER_ICD3=1 -q -p$(MP_PROCESSOR_OPTION) -u  -l\\\"${OBJECTDIR}/FurnaceMonitor1821.lst\\\" -e\\\"${OBJECTDIR}/FurnaceMonitor1821.err\\\" $(ASM_OPTIONS)   -o\\\"${OBJECTDIR}/FurnaceMonitor1821.o\\\" \\\"FurnaceMonitor1821.asm\\\" 
	@${DEP_GEN} -d "${OBJECTDIR}/FurnaceMonitor1821.o"
	@${FIXDEPS} "${OBJECTDIR}/FurnaceMonitor1821.o.d" $(SILENT) -rsi ${MP_AS_DIR} -c18 
	
else
${OBJECTDIR}/LCD.o: LCD.asm  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} ${OBJECTDIR} 
	@${RM} ${OBJECTDIR}/LCD.o.d 
	@${RM} ${OBJECTDIR}/LCD.o 
	@${FIXDEPS} dummy.d -e "${OBJECTDIR}/LCD.err" $(SILENT) -c ${MP_AS} $(MP_EXTRA_AS_PRE) -q -p$(MP_PROCESSOR_OPTION) -u  -l\\\"${OBJECTDIR}/LCD.lst\\\" -e\\\"${OBJECTDIR}/LCD.err\\\" $(ASM_OPTIONS)   -o\\\"${OBJECTDIR}/LCD.o\\\" \\\"LCD.asm\\\" 
	@${DEP_GEN} -d "${OBJECTDIR}/LCD.o"
	@${FIXDEPS} "${OBJECTDIR}/LCD.o.d" $(SILENT) -rsi ${MP_AS_DIR} -c18 
	
${OBJECTDIR}/_divS16.o: _divS16.asm  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} ${OBJECTDIR} 
	@${RM} ${OBJECTDIR}/_divS16.o.d 
	@${RM} ${OBJECTDIR}/_divS16.o 
	@${FIXDEPS} dummy.d -e "${OBJECTDIR}/_divS16.err" $(SILENT) -c ${MP_AS} $(MP_EXTRA_AS_PRE) -q -p$(MP_PROCESSOR_OPTION) -u  -l\\\"${OBJECTDIR}/_divS16.lst\\\" -e\\\"${OBJECTDIR}/_divS16.err\\\" $(ASM_OPTIONS)   -o\\\"${OBJECTDIR}/_divS16.o\\\" \\\"_divS16.asm\\\" 
	@${DEP_GEN} -d "${OBJECTDIR}/_divS16.o"
	@${FIXDEPS} "${OBJECTDIR}/_divS16.o.d" $(SILENT) -rsi ${MP_AS_DIR} -c18 
	
${OBJECTDIR}/binBCD16.o: binBCD16.asm  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} ${OBJECTDIR} 
	@${RM} ${OBJECTDIR}/binBCD16.o.d 
	@${RM} ${OBJECTDIR}/binBCD16.o 
	@${FIXDEPS} dummy.d -e "${OBJECTDIR}/binBCD16.err" $(SILENT) -c ${MP_AS} $(MP_EXTRA_AS_PRE) -q -p$(MP_PROCESSOR_OPTION) -u  -l\\\"${OBJECTDIR}/binBCD16.lst\\\" -e\\\"${OBJECTDIR}/binBCD16.err\\\" $(ASM_OPTIONS)   -o\\\"${OBJECTDIR}/binBCD16.o\\\" \\\"binBCD16.asm\\\" 
	@${DEP_GEN} -d "${OBJECTDIR}/binBCD16.o"
	@${FIXDEPS} "${OBJECTDIR}/binBCD16.o.d" $(SILENT) -rsi ${MP_AS_DIR} -c18 
	
${OBJECTDIR}/del100us.o: del100us.asm  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} ${OBJECTDIR} 
	@${RM} ${OBJECTDIR}/del100us.o.d 
	@${RM} ${OBJECTDIR}/del100us.o 
	@${FIXDEPS} dummy.d -e "${OBJECTDIR}/del100us.err" $(SILENT) -c ${MP_AS} $(MP_EXTRA_AS_PRE) -q -p$(MP_PROCESSOR_OPTION) -u  -l\\\"${OBJECTDIR}/del100us.lst\\\" -e\\\"${OBJECTDIR}/del100us.err\\\" $(ASM_OPTIONS)   -o\\\"${OBJECTDIR}/del100us.o\\\" \\\"del100us.asm\\\" 
	@${DEP_GEN} -d "${OBJECTDIR}/del100us.o"
	@${FIXDEPS} "${OBJECTDIR}/del100us.o.d" $(SILENT) -rsi ${MP_AS_DIR} -c18 
	
${OBJECTDIR}/del10us.o: del10us.asm  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} ${OBJECTDIR} 
	@${RM} ${OBJECTDIR}/del10us.o.d 
	@${RM} ${OBJECTDIR}/del10us.o 
	@${FIXDEPS} dummy.d -e "${OBJECTDIR}/del10us.err" $(SILENT) -c ${MP_AS} $(MP_EXTRA_AS_PRE) -q -p$(MP_PROCESSOR_OPTION) -u  -l\\\"${OBJECTDIR}/del10us.lst\\\" -e\\\"${OBJECTDIR}/del10us.err\\\" $(ASM_OPTIONS)   -o\\\"${OBJECTDIR}/del10us.o\\\" \\\"del10us.asm\\\" 
	@${DEP_GEN} -d "${OBJECTDIR}/del10us.o"
	@${FIXDEPS} "${OBJECTDIR}/del10us.o.d" $(SILENT) -rsi ${MP_AS_DIR} -c18 
	
${OBJECTDIR}/del1ms.o: del1ms.asm  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} ${OBJECTDIR} 
	@${RM} ${OBJECTDIR}/del1ms.o.d 
	@${RM} ${OBJECTDIR}/del1ms.o 
	@${FIXDEPS} dummy.d -e "${OBJECTDIR}/del1ms.err" $(SILENT) -c ${MP_AS} $(MP_EXTRA_AS_PRE) -q -p$(MP_PROCESSOR_OPTION) -u  -l\\\"${OBJECTDIR}/del1ms.lst\\\" -e\\\"${OBJECTDIR}/del1ms.err\\\" $(ASM_OPTIONS)   -o\\\"${OBJECTDIR}/del1ms.o\\\" \\\"del1ms.asm\\\" 
	@${DEP_GEN} -d "${OBJECTDIR}/del1ms.o"
	@${FIXDEPS} "${OBJECTDIR}/del1ms.o.d" $(SILENT) -rsi ${MP_AS_DIR} -c18 
	
${OBJECTDIR}/masterReset.o: masterReset.asm  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} ${OBJECTDIR} 
	@${RM} ${OBJECTDIR}/masterReset.o.d 
	@${RM} ${OBJECTDIR}/masterReset.o 
	@${FIXDEPS} dummy.d -e "${OBJECTDIR}/masterReset.err" $(SILENT) -c ${MP_AS} $(MP_EXTRA_AS_PRE) -q -p$(MP_PROCESSOR_OPTION) -u  -l\\\"${OBJECTDIR}/masterReset.lst\\\" -e\\\"${OBJECTDIR}/masterReset.err\\\" $(ASM_OPTIONS)   -o\\\"${OBJECTDIR}/masterReset.o\\\" \\\"masterReset.asm\\\" 
	@${DEP_GEN} -d "${OBJECTDIR}/masterReset.o"
	@${FIXDEPS} "${OBJECTDIR}/masterReset.o.d" $(SILENT) -rsi ${MP_AS_DIR} -c18 
	
${OBJECTDIR}/pollStatus.o: pollStatus.asm  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} ${OBJECTDIR} 
	@${RM} ${OBJECTDIR}/pollStatus.o.d 
	@${RM} ${OBJECTDIR}/pollStatus.o 
	@${FIXDEPS} dummy.d -e "${OBJECTDIR}/pollStatus.err" $(SILENT) -c ${MP_AS} $(MP_EXTRA_AS_PRE) -q -p$(MP_PROCESSOR_OPTION) -u  -l\\\"${OBJECTDIR}/pollStatus.lst\\\" -e\\\"${OBJECTDIR}/pollStatus.err\\\" $(ASM_OPTIONS)   -o\\\"${OBJECTDIR}/pollStatus.o\\\" \\\"pollStatus.asm\\\" 
	@${DEP_GEN} -d "${OBJECTDIR}/pollStatus.o"
	@${FIXDEPS} "${OBJECTDIR}/pollStatus.o.d" $(SILENT) -rsi ${MP_AS_DIR} -c18 
	
${OBJECTDIR}/readBYTE.o: readBYTE.asm  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} ${OBJECTDIR} 
	@${RM} ${OBJECTDIR}/readBYTE.o.d 
	@${RM} ${OBJECTDIR}/readBYTE.o 
	@${FIXDEPS} dummy.d -e "${OBJECTDIR}/readBYTE.err" $(SILENT) -c ${MP_AS} $(MP_EXTRA_AS_PRE) -q -p$(MP_PROCESSOR_OPTION) -u  -l\\\"${OBJECTDIR}/readBYTE.lst\\\" -e\\\"${OBJECTDIR}/readBYTE.err\\\" $(ASM_OPTIONS)   -o\\\"${OBJECTDIR}/readBYTE.o\\\" \\\"readBYTE.asm\\\" 
	@${DEP_GEN} -d "${OBJECTDIR}/readBYTE.o"
	@${FIXDEPS} "${OBJECTDIR}/readBYTE.o.d" $(SILENT) -rsi ${MP_AS_DIR} -c18 
	
${OBJECTDIR}/sendByte.o: sendByte.asm  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} ${OBJECTDIR} 
	@${RM} ${OBJECTDIR}/sendByte.o.d 
	@${RM} ${OBJECTDIR}/sendByte.o 
	@${FIXDEPS} dummy.d -e "${OBJECTDIR}/sendByte.err" $(SILENT) -c ${MP_AS} $(MP_EXTRA_AS_PRE) -q -p$(MP_PROCESSOR_OPTION) -u  -l\\\"${OBJECTDIR}/sendByte.lst\\\" -e\\\"${OBJECTDIR}/sendByte.err\\\" $(ASM_OPTIONS)   -o\\\"${OBJECTDIR}/sendByte.o\\\" \\\"sendByte.asm\\\" 
	@${DEP_GEN} -d "${OBJECTDIR}/sendByte.o"
	@${FIXDEPS} "${OBJECTDIR}/sendByte.o.d" $(SILENT) -rsi ${MP_AS_DIR} -c18 
	
${OBJECTDIR}/showBCD.o: showBCD.asm  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} ${OBJECTDIR} 
	@${RM} ${OBJECTDIR}/showBCD.o.d 
	@${RM} ${OBJECTDIR}/showBCD.o 
	@${FIXDEPS} dummy.d -e "${OBJECTDIR}/showBCD.err" $(SILENT) -c ${MP_AS} $(MP_EXTRA_AS_PRE) -q -p$(MP_PROCESSOR_OPTION) -u  -l\\\"${OBJECTDIR}/showBCD.lst\\\" -e\\\"${OBJECTDIR}/showBCD.err\\\" $(ASM_OPTIONS)   -o\\\"${OBJECTDIR}/showBCD.o\\\" \\\"showBCD.asm\\\" 
	@${DEP_GEN} -d "${OBJECTDIR}/showBCD.o"
	@${FIXDEPS} "${OBJECTDIR}/showBCD.o.d" $(SILENT) -rsi ${MP_AS_DIR} -c18 
	
${OBJECTDIR}/del1ms32.o: del1ms32.asm  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} ${OBJECTDIR} 
	@${RM} ${OBJECTDIR}/del1ms32.o.d 
	@${RM} ${OBJECTDIR}/del1ms32.o 
	@${FIXDEPS} dummy.d -e "${OBJECTDIR}/del1ms32.err" $(SILENT) -c ${MP_AS} $(MP_EXTRA_AS_PRE) -q -p$(MP_PROCESSOR_OPTION) -u  -l\\\"${OBJECTDIR}/del1ms32.lst\\\" -e\\\"${OBJECTDIR}/del1ms32.err\\\" $(ASM_OPTIONS)   -o\\\"${OBJECTDIR}/del1ms32.o\\\" \\\"del1ms32.asm\\\" 
	@${DEP_GEN} -d "${OBJECTDIR}/del1ms32.o"
	@${FIXDEPS} "${OBJECTDIR}/del1ms32.o.d" $(SILENT) -rsi ${MP_AS_DIR} -c18 
	
${OBJECTDIR}/init1821.o: init1821.asm  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} ${OBJECTDIR} 
	@${RM} ${OBJECTDIR}/init1821.o.d 
	@${RM} ${OBJECTDIR}/init1821.o 
	@${FIXDEPS} dummy.d -e "${OBJECTDIR}/init1821.err" $(SILENT) -c ${MP_AS} $(MP_EXTRA_AS_PRE) -q -p$(MP_PROCESSOR_OPTION) -u  -l\\\"${OBJECTDIR}/init1821.lst\\\" -e\\\"${OBJECTDIR}/init1821.err\\\" $(ASM_OPTIONS)   -o\\\"${OBJECTDIR}/init1821.o\\\" \\\"init1821.asm\\\" 
	@${DEP_GEN} -d "${OBJECTDIR}/init1821.o"
	@${FIXDEPS} "${OBJECTDIR}/init1821.o.d" $(SILENT) -rsi ${MP_AS_DIR} -c18 
	
${OBJECTDIR}/getTemp.o: getTemp.asm  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} ${OBJECTDIR} 
	@${RM} ${OBJECTDIR}/getTemp.o.d 
	@${RM} ${OBJECTDIR}/getTemp.o 
	@${FIXDEPS} dummy.d -e "${OBJECTDIR}/getTemp.err" $(SILENT) -c ${MP_AS} $(MP_EXTRA_AS_PRE) -q -p$(MP_PROCESSOR_OPTION) -u  -l\\\"${OBJECTDIR}/getTemp.lst\\\" -e\\\"${OBJECTDIR}/getTemp.err\\\" $(ASM_OPTIONS)   -o\\\"${OBJECTDIR}/getTemp.o\\\" \\\"getTemp.asm\\\" 
	@${DEP_GEN} -d "${OBJECTDIR}/getTemp.o"
	@${FIXDEPS} "${OBJECTDIR}/getTemp.o.d" $(SILENT) -rsi ${MP_AS_DIR} -c18 
	
${OBJECTDIR}/SecondDS.o: SecondDS.asm  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} ${OBJECTDIR} 
	@${RM} ${OBJECTDIR}/SecondDS.o.d 
	@${RM} ${OBJECTDIR}/SecondDS.o 
	@${FIXDEPS} dummy.d -e "${OBJECTDIR}/SecondDS.err" $(SILENT) -c ${MP_AS} $(MP_EXTRA_AS_PRE) -q -p$(MP_PROCESSOR_OPTION) -u  -l\\\"${OBJECTDIR}/SecondDS.lst\\\" -e\\\"${OBJECTDIR}/SecondDS.err\\\" $(ASM_OPTIONS)   -o\\\"${OBJECTDIR}/SecondDS.o\\\" \\\"SecondDS.asm\\\" 
	@${DEP_GEN} -d "${OBJECTDIR}/SecondDS.o"
	@${FIXDEPS} "${OBJECTDIR}/SecondDS.o.d" $(SILENT) -rsi ${MP_AS_DIR} -c18 
	
${OBJECTDIR}/convert1.o: convert1.asm  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} ${OBJECTDIR} 
	@${RM} ${OBJECTDIR}/convert1.o.d 
	@${RM} ${OBJECTDIR}/convert1.o 
	@${FIXDEPS} dummy.d -e "${OBJECTDIR}/convert1.err" $(SILENT) -c ${MP_AS} $(MP_EXTRA_AS_PRE) -q -p$(MP_PROCESSOR_OPTION) -u  -l\\\"${OBJECTDIR}/convert1.lst\\\" -e\\\"${OBJECTDIR}/convert1.err\\\" $(ASM_OPTIONS)   -o\\\"${OBJECTDIR}/convert1.o\\\" \\\"convert1.asm\\\" 
	@${DEP_GEN} -d "${OBJECTDIR}/convert1.o"
	@${FIXDEPS} "${OBJECTDIR}/convert1.o.d" $(SILENT) -rsi ${MP_AS_DIR} -c18 
	
${OBJECTDIR}/convert2.o: convert2.asm  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} ${OBJECTDIR} 
	@${RM} ${OBJECTDIR}/convert2.o.d 
	@${RM} ${OBJECTDIR}/convert2.o 
	@${FIXDEPS} dummy.d -e "${OBJECTDIR}/convert2.err" $(SILENT) -c ${MP_AS} $(MP_EXTRA_AS_PRE) -q -p$(MP_PROCESSOR_OPTION) -u  -l\\\"${OBJECTDIR}/convert2.lst\\\" -e\\\"${OBJECTDIR}/convert2.err\\\" $(ASM_OPTIONS)   -o\\\"${OBJECTDIR}/convert2.o\\\" \\\"convert2.asm\\\" 
	@${DEP_GEN} -d "${OBJECTDIR}/convert2.o"
	@${FIXDEPS} "${OBJECTDIR}/convert2.o.d" $(SILENT) -rsi ${MP_AS_DIR} -c18 
	
${OBJECTDIR}/calcDeltas.o: calcDeltas.asm  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} ${OBJECTDIR} 
	@${RM} ${OBJECTDIR}/calcDeltas.o.d 
	@${RM} ${OBJECTDIR}/calcDeltas.o 
	@${FIXDEPS} dummy.d -e "${OBJECTDIR}/calcDeltas.err" $(SILENT) -c ${MP_AS} $(MP_EXTRA_AS_PRE) -q -p$(MP_PROCESSOR_OPTION) -u  -l\\\"${OBJECTDIR}/calcDeltas.lst\\\" -e\\\"${OBJECTDIR}/calcDeltas.err\\\" $(ASM_OPTIONS)   -o\\\"${OBJECTDIR}/calcDeltas.o\\\" \\\"calcDeltas.asm\\\" 
	@${DEP_GEN} -d "${OBJECTDIR}/calcDeltas.o"
	@${FIXDEPS} "${OBJECTDIR}/calcDeltas.o.d" $(SILENT) -rsi ${MP_AS_DIR} -c18 
	
${OBJECTDIR}/showExtremes.o: showExtremes.asm  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} ${OBJECTDIR} 
	@${RM} ${OBJECTDIR}/showExtremes.o.d 
	@${RM} ${OBJECTDIR}/showExtremes.o 
	@${FIXDEPS} dummy.d -e "${OBJECTDIR}/showExtremes.err" $(SILENT) -c ${MP_AS} $(MP_EXTRA_AS_PRE) -q -p$(MP_PROCESSOR_OPTION) -u  -l\\\"${OBJECTDIR}/showExtremes.lst\\\" -e\\\"${OBJECTDIR}/showExtremes.err\\\" $(ASM_OPTIONS)   -o\\\"${OBJECTDIR}/showExtremes.o\\\" \\\"showExtremes.asm\\\" 
	@${DEP_GEN} -d "${OBJECTDIR}/showExtremes.o"
	@${FIXDEPS} "${OBJECTDIR}/showExtremes.o.d" $(SILENT) -rsi ${MP_AS_DIR} -c18 
	
${OBJECTDIR}/FurnaceMonitor1821.o: FurnaceMonitor1821.asm  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} ${OBJECTDIR} 
	@${RM} ${OBJECTDIR}/FurnaceMonitor1821.o.d 
	@${RM} ${OBJECTDIR}/FurnaceMonitor1821.o 
	@${FIXDEPS} dummy.d -e "${OBJECTDIR}/FurnaceMonitor1821.err" $(SILENT) -c ${MP_AS} $(MP_EXTRA_AS_PRE) -q -p$(MP_PROCESSOR_OPTION) -u  -l\\\"${OBJECTDIR}/FurnaceMonitor1821.lst\\\" -e\\\"${OBJECTDIR}/FurnaceMonitor1821.err\\\" $(ASM_OPTIONS)   -o\\\"${OBJECTDIR}/FurnaceMonitor1821.o\\\" \\\"FurnaceMonitor1821.asm\\\" 
	@${DEP_GEN} -d "${OBJECTDIR}/FurnaceMonitor1821.o"
	@${FIXDEPS} "${OBJECTDIR}/FurnaceMonitor1821.o.d" $(SILENT) -rsi ${MP_AS_DIR} -c18 
	
endif

# ------------------------------------------------------------------------------------
# Rules for buildStep: link
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
dist/${CND_CONF}/${IMAGE_TYPE}/FurnaceMonitor1821.X.${IMAGE_TYPE}.${OUTPUT_SUFFIX}: ${OBJECTFILES}  nbproject/Makefile-${CND_CONF}.mk    1821PICel-87.lkr
	@${MKDIR} dist/${CND_CONF}/${IMAGE_TYPE} 
	${MP_LD} $(MP_EXTRA_LD_PRE) "1821PICel-87.lkr"  -p$(MP_PROCESSOR_OPTION)  -w -x -u_DEBUG -z__ICD2RAM=1 -m"${DISTDIR}/${PROJECTNAME}.${IMAGE_TYPE}.map"   -z__MPLAB_BUILD=1  -z__MPLAB_DEBUG=1 -z__MPLAB_DEBUGGER_ICD3=1 $(MP_LINKER_DEBUG_OPTION) -odist/${CND_CONF}/${IMAGE_TYPE}/FurnaceMonitor1821.X.${IMAGE_TYPE}.${OUTPUT_SUFFIX}  ${OBJECTFILES_QUOTED_IF_SPACED}     
else
dist/${CND_CONF}/${IMAGE_TYPE}/FurnaceMonitor1821.X.${IMAGE_TYPE}.${OUTPUT_SUFFIX}: ${OBJECTFILES}  nbproject/Makefile-${CND_CONF}.mk   1821PICel-87.lkr
	@${MKDIR} dist/${CND_CONF}/${IMAGE_TYPE} 
	${MP_LD} $(MP_EXTRA_LD_PRE) "1821PICel-87.lkr"  -p$(MP_PROCESSOR_OPTION)  -w  -m"${DISTDIR}/${PROJECTNAME}.${IMAGE_TYPE}.map"   -z__MPLAB_BUILD=1  -odist/${CND_CONF}/${IMAGE_TYPE}/FurnaceMonitor1821.X.${IMAGE_TYPE}.${DEBUGGABLE_SUFFIX}  ${OBJECTFILES_QUOTED_IF_SPACED}     
endif


# Subprojects
.build-subprojects:


# Subprojects
.clean-subprojects:

# Clean Targets
.clean-conf: ${CLEAN_SUBPROJECTS}
	${RM} -r build/Blue
	${RM} -r dist/Blue

# Enable dependency checking
.dep.inc: .depcheck-impl

DEPFILES=$(shell "${PATH_TO_IDE_BIN}"mplabwildcard ${POSSIBLE_DEPFILES})
ifneq (${DEPFILES},)
include ${DEPFILES}
endif
