;
;   getTemp
;

            include     Processor.inc
            include     DS1821.inc
            radix       DEC

            global      getTemp
		    extern		MasterReset,PollStatus,SendByte,ReadByte
            extern      Delay1ms
            ;extern      tempmin
            ;extern      tempmax
;
            extern      tempbuf
            extern      sign
            extern      countrem
            extern      countdeg

PROG        code

getTemp:
            call        MasterReset
			;                       SendByte(STARTCONV);
            movlw       STARTCONV
            call        SendByte
			;
			;                       //Hier Status Polling f�r Ende der Messung
			;                       PollStatus();
            call        PollStatus
			;
			;                       MasterReset();
            call        MasterReset
			;                       SendByte(READTEMP);
            movlw       READTEMP
            call        SendByte
			;                       ReadByte(8);       //Temperatur lesen
            movlw       .8
            call        ReadByte
			;                       sign=(int)(tempbuf&0xFF); //Nur die unteren 8 Bits nehmen !
            movf        tempbuf,W
            movwf       sign
			;
			;                       //Die beiden 9 Bit Register lesen
			;                       MasterReset();
            call        MasterReset
			;                       SendByte(READCOUNT);
            movlw       READCOUNT
            call        SendByte
			;                       ReadByte(9);       //Remain lesen
            movlw       .9
            call        ReadByte
			;                       countrem=tempbuf;
            movf        tempbuf,W
            movwf       countrem
            movf        tempbuf+1,W
            movwf       countrem+1
			;
			;                       MasterReset();
            call        MasterReset
			;                       SendByte(LOADREM); //Zweites Register laden Load second register
            movlw       LOADCOUNT
            call        SendByte
			;
			;                       MasterReset();
            call        MasterReset
			;                       SendByte(READCOUNT);
            movlw       READCOUNT
            call        SendByte
			;                       ReadByte(9);       //Degree lesen Read Degree
            movlw       .9
            call        ReadByte
			;                       countdeg=tempbuf;
            movf        tempbuf,W
            movwf       countdeg
            movf        tempbuf+1,W
            movwf       countdeg+1

            return
            end