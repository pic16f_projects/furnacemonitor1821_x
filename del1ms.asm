  ; FILE delay.c
			;// Datei: delay.c
			;// Verzögerungsschleifen
			;//
			;// Holger Klabunde 
			;// 01.05.2001
			;// Compiler CC5x
			;
			;//#include "prozess.h"      /* Der verwendete Prozessor */
			;#include "protos.h"
			;
			;//Für Taktfrequenz des PIC = 4MHz
			;//Ein Taktzyklus=1us
			;//delay<=255 !!
			;unsigned char del;
			;
			;//Assemblerbefehle zwischen #asm und #endasm !! klein !! schreiben
			;//Kollidiert sonst mit inline.h
			;

;	$Revision: 0.2 $  $Date: 2005/11/20 01:58:20 $

		include		Processor.inc
		global		Delay1ms

DELS	udata_ovr
;delay_2     EQU   0x0F
;delay_2		res		1
;delay       EQU   0x12	;*****************
delay		res		1
;del         EQU   0x1D
del			res		1

			;void Delay1ms(unsigned char delay)
			;{
DEL		code
Delay1ms
	movwf delay
			; del=delay;
	movf  delay,W
	movwf del
			;#asm
			;
			;DLMS4M1
			;   movlw 0xF9          //1
m008
 IFDEF KHZ32
    movlw   .2
 ELSE
	movlw .249
 ENDIF
			;   movwf FSR           //1
	movwf FSR
			;
			;DLMS4M2                //4 cycles
			;     nop               //1
m009	nop  
			;     decfsz FSR        //1
	decfsz FSR,F
			;     goto  DLMS4M2     //2
	goto  m009
			;
			;   decfsz del     //1
	decfsz del,F
			;   goto DLMS4M1        //2
	goto  m008
			;#endasm
			;}
	return
	end
