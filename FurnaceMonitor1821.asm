; from:
; FILE ds1821io.c

            include     Processor.inc
            include     DS1821.inc
            radix       DEC

    if PROC == 84
            messg   "Configuring for PROC==84"
            __config    _XT_OSC & _WDT_OFF & _PWRTE_ON & _CP_OFF
    endif

    IF      PROC == 627
            messg   "Configuring for PROC==627"
            __config    _XT_OSC & _WDT_OFF & _PWRTE_ON & _CP_OFF & _LVP_OFF & _BODEN_OFF
    ENDIF

    IF      PROC == 88
            messg   "Configuring for PROC==88"
            __config    _CONFIG1, _CP_OFF & _WDT_OFF & _PWRTE_ON & _LVP_OFF & _BOREN_OFF & _INTRC_IO & _MCLR_ON & _CPD_OFF & _WRT_OFF
            __config    _CONFIG2, _FCMEN_OFF & _IESO_ON
    ENDIF

    IF      PROC == 819
            messg   "Configuring for PROC==819"
            __config    _XT_OSC & _WDT_OFF & _PWRTE_ON & _CP_OFF & _LVP_OFF & _BODEN_OFF & _MCLR_ON
    ENDIF

SETOSC      MACRO   s
     IF      PROC == 88
            ;                       Set clock
            errorlevel	-302
            banksel     OSCCON
            movlw       s
            movwf       OSCCON
            nop
            banksel     PORTA
    ENDIF
            ENDM

#define     LCDON       bsf PORTA,2
#define     LCDOFF      bcf PORTA,2

            extern      BinToBCD16,ShowBCD
		    extern		Delay1ms,Delay10us,Delay100us,Delay1ms32
		    extern		_divS16_16
		    extern		arg1_8,arg2_8
		    extern		MasterReset,PollStatus,SendByte,ReadByte
		    extern      LCDInit,LCDWriteByte,LCDCls,LCDPos
            global      i,position,buff,buff_2,dat,zeile
            extern      calcDeltas

		    global		temp,tempbuf
            global      countrem,countdeg
            global      countrem2,countdeg2
            global      sign
            global      tempbuf2,sign2,temp2
            global      min1,max1,min2,max2
            global      delta,mindelta,maxdelta

            extern      init1821, getTemp
            extern      init18212, getTemp2
            ;extern      preDisplay
            extern      showExtremes
            extern      convert1,convert2

            udata
RA2         EQU         2
RA3         EQU         3
tempbuf		res		    2
sign		res		    1
temp		res		    2
countrem	res		    2
countdeg	res		    2
;tempmin		res		    2
;tempmax		res		    2
i_2			res		    1
tempbuf2    res         2
sign2       res         1
temp2       res         2
countrem2   res		    2
countdeg2   res		    2
min1        res         2
max1        res         2
min2        res         2
max2        res         2
delta       res         2
mindelta    res         2
maxdelta    res         2

SHSP		udata_ovr
by			res		    1
tictoc      res         1

SHSP		udata_ovr
zeile       res		    1
position	res		    1
buff_2      res		    1

SHSP		udata_ovr
i			res		    1

LCDIO       udata
dat         res			1
buff        res			1

STARTUP     code
            goto    main


  ; FILE ds1821.c
			;//Datei: DS1821.C
			;// Hauptprogramm
			;//
			;// Pr�zisionsthermometer
			;// mit PIC16F84 und DS1821
			;//
			;// Holger Klabunde
			;// Copyright 2000
			;// 24.04.2001
			;// Compiler CC5x
			;
			;// Verwendete LCD-Anzeige 2x16 Zeichen oder 2x8 Zeichen
			;
			;#include "prozess.h"      /* Der verwendete Prozessor */
			;
			;#include "ds1821.h"
			;#include "protos.h"
			;#include <math16.h>
			;
			;//Hier mal was ungew�hnliches bei CC5X !
			;//Statt einer Linkerdatei m�ssen alle Module #included werden
			;#include "delay.c"
			;#include "ds1821io.c"
			;#include "binbcd16.c"
			;#include "lcd.c"
			;
			; // Watchdog off, Power up Timer on, XT Oscillator, Code Protect off
			;#pragma config |= 0x3FF1 //Configuration Word
			;
			;//Globale Flags und R�ckgabevariablen
			;
			;//long ist 16Bit !
			;int sign;       //Vorzeichen und 7Bit Temperaturwert
			;long temp;     //Temperatur
			;long countrem; //9 Bit Register 1
			;long countdeg; //9 Bit Register 2
			;long tempbuf;  //�bergabevariable f�r Anzeige
			;long tempmin;  //Kleinste gemessene Temperatur
			;long tempmax;  //gr��te gemessene Temperatur
			;

PROG	code
			;
			;/* Hauptprogramm */
			;void main()
			;{
main

			;                       unsigned char i;
			;
			;                       OPTION=0b11010110;	 //Prescaler 1:128 und Intern Clock f�r TMR0
            ; Slow speed for now
            SETOSC      h'06'

            ; Option: PORTB Pullups enabled, INTEDG, CLK0, lo-hi, 1:128 (~4.1 sec rollover)
            movlw       h'D7'   ; WB8RCR was D6 changed to D7 to slow TMR0
            errorlevel	-302
            banksel     OPTION_REG
        	movwf       OPTION_REG
			; 
			;   // Der Takt des PIC betr�gt 4MHz (The clock of the PIC is 4MHz)
			;   // Ein Maschinenzyklus damit 1us (A machine cycle so 1us)
			;
			;                       TRISA=0x0F; // PortA 	alles Eing�nge au�er RA4 (all inputs except RA4)
            banksel     TRISA
            movlw       .15
            movlw       H'03'       ;;;;;;;;;;; Changed RA2 to output for power
            movwf       TRISA
			;                       TRISB=0b00000000; // PortB 	alles Ausg�nge au�er PB6 (all outputs except PB6)
            clrf        TRISB
			;
			;                       PORTB=0x20;    //Definierten Zustand einstellen (Set defined state)
            movlw       .32
            errorlevel	+302
            banksel     PORTB
            movwf       PORTB
			;                       PORTA=0x1F;    //Pullup Ra4
            movlw       .31
            movwf       PORTA       ;;;;;;;;;  We want RA3 high so this is good
            LCDON
			;
			;                       LCDInit(); //LCD Anzeige einstellen
            call        LCDInit
			;
			;                       LCDCls();  //Anzeige l�schen
            call        LCDCls
			;
			;                       LCDWriteByte('1');
            movlw       .49
            call        LCDWriteByte
			;                       LCDWriteByte('8');
            movlw       .56
            call        LCDWriteByte
			;                       LCDWriteByte('2');
            movlw       .50
            call        LCDWriteByte
			;                       LCDWriteByte('1');
            movlw       .49
            call        LCDWriteByte
			;                       Delay1ms(255);
            movlw       .255
            call        Delay1ms32
			;                       Delay1ms(255);
            movlw       .255
            call        Delay1ms32
			;                       Delay1ms(255);
            movlw       .255
            call        Delay1ms32
			;                       LCDCls();
            call        LCDCls
            LCDOFF
			;
;=================================== Read Temp ================================
            ; Entering time sensitive stuff so turn up the gas
            SETOSC      H'66'

            call        init1821
            call        init18212
			; 
			;                       while(1) // Anfang Programm Endlosschleife
			;                        {
m040
            call        getTemp
            call        getTemp2
    ;
    ; The following are not time-sensitive, but there are no delay loops
    ; so it actually costs more current to run them slow
            call        convert1
            call        convert2
            call        calcDeltas

;=================================== Convert Temp =============================
            ; Time sensitive stuff done, back to slow
            SETOSC      h'06'

;========================== Hang around wasting time ==========================

            movlw       b'00110001' ; Timer 1 on, 1:8 prescale
            movwf       T1CON
            movlw       0x07
            movwf       TMR1H
            movlw       0x00
            movwf       TMR1L
            bcf         PIR1,TMR1IF

wait0
            btfsc       PORTA,0
            goto        continue

            LCDON
            call        showExtremes
            movlw       .45
            call        Delay1ms
            LCDOFF

continue
            btfss       PIR1,TMR1IF
            goto        wait0

            SETOSC      H'66'

            goto  m040
			;}
			;
IRQ         code
interrupt:
            nop
            bcf         INTCON,TMR0IF

            retfie
	end
