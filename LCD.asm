            include     Processor.inc
            include     DS1821.inc
            radix       DEC

    ; $Revision: 1.2 $ $Date: 2007/05/30 14:24:28 $

			global	LCDInit,LCDPos,LCDWriteByte,LCDCls
            extern  Delay1ms32
            extern  i,position,buff,buff_2,dat,zeile

LCDCS       equ         4
LCDRS       equ         6
            udata
lcdport     res		    1   ; Shadow register for PORTB

;*************************************************************************
;    MACROS
;*************************************************************************
;   chip select low
LCDCS_OFF   MACRO
            bcf     lcdport,LCDCS
            movf    lcdport,W
            movwf   PORTB
            endM
;   chip select high
LCDCS_ON    MACRO
            bsf     lcdport,LCDCS
            movf    lcdport,W
            movwf   PORTB
            endM
;   register select low
LCDRS_OFF   MACRO
            bcf     lcdport,LCDRS
            movf    lcdport,W
            movwf   PORTB
            endM
;   register select high
LCDRS_ON    MACRO
            bsf     lcdport,LCDRS
            movf    lcdport,W
            movwf   PORTB
            endM
;*************************************************************************

LCD		    code
; From:
; FILE lcd.c

LCDWriteByte
            movwf   dat
			;    unsigned char buff;
			; 
			;    // PORTB.5 = 1;
			;
			;    lcdport=PORTB;   // Get old value
	banksel PORTB
	movf  PORTB,W
	movwf lcdport
			;    lcdport = lcdport & 0xfd; // R/W = W
	bcf   lcdport,F
			; 
			;    LCDCS_OFF();
	bcf   lcdport,4
	movf  lcdport,W
	movwf PORTB
			;
			;    buff=dat>>4;   //High Nibble des Bytes
	swapf dat,W
	andlw .15
	movwf buff
			;    lcdport=lcdport&0xF0;	// Delete bits 0-3 from old value
	movlw .240
	andwf lcdport,F
			;    lcdport|=buff;		// OR in the high nybble of the data
	movf  buff,W
	iorwf lcdport,F
			;
			;    LCDCS_ON();	      // This will write the high nybble
	bsf   lcdport,4
	movf  lcdport,W
	movwf PORTB
			;    LCDCS_OFF();	
	bcf   lcdport,4
	movf  lcdport,W
	movwf PORTB
			;
			;    buff=dat&0x0F;        // Now pick off the low nybble
	movlw .15
	andwf dat,W
	movwf buff
			;    lcdport=lcdport&0xF0;	// Loose bits 0-3 from the old value
	movlw .240
	andwf lcdport,F
			;    lcdport|=buff;		// OR in the low nybble
	movf  buff,W
	iorwf lcdport,F
			;
			;    LCDCS_ON();	      // and write it as before
	bsf   lcdport,4
	movf  lcdport,W
	movwf PORTB
			;    LCDCS_OFF();	
	bcf   lcdport,4
	movf  lcdport,W
	movwf PORTB
			;    Delay1ms32(1);
	movlw .1
	goto  Delay1ms32
			;}
			;
			;
			;//****************************************************
			;// Position the LCD cursor.  For two line display.
			;//****************************************************
			;void LCDPos(unsigned char zeile,unsigned char position)
			;{
LCDPos
	movwf position
			;    unsigned char buff;
			;
			;    LCDRS_OFF(); // Switch to control register
	bcf   lcdport,6
	movf  lcdport,W
    banksel PORTB
	movwf PORTB
			;
			;    // 0x80 because Display Adress Set.
			;    // First Position is  1, not 0.
			;    // Offset's
			;    // Line 1 -> 0x00+0x80-0x01=0x7F
			;    // Line 2 -> 0x40+0x80-0x01=0xBF 
			;
			;    switch(zeile)
	movf  zeile,W
	XORLW .1
	btfsc STATUS,Z
	goto  m033
	XORLW .3
	btfsc STATUS,Z
	goto  m034
	goto  m035
			;	{
			;	case 1: buff=0x7F+position; break;
m033	movlw .127
	addwf position,W
	movwf buff_2
	goto  m036
			;	case 2: buff=0xBF+position; break;
m034	movlw .191
	addwf position,W
	movwf buff_2
	goto  m036
			;	default : break;
m035	goto  m036
			;	}
			;
			;    LCDWriteByte(buff); 
m036	movf  buff_2,W
	call  LCDWriteByte
			;    LCDRS_ON(); // Switch back to data register
	bsf   lcdport,6
	movf  lcdport,W
    banksel PORTB
	movwf PORTB
			;    Wait();
	movlw .5
	goto  Delay1ms32
			;}
			;
			;//*****************************************
			;// The LCD deletes announcement cursor stands
			;// then on position 1, line 1 applies also
			;// to announcements of several lines
			;//*****************************************
			;void LCDCls()
			;{
LCDCls
			;    LCDRS_OFF(); //Controlregister
	bcf   lcdport,6
	movf  lcdport,W
    banksel PORTB
	movwf PORTB
			;    LCDWriteByte(0x01); //CLS
	movlw .1
	call  LCDWriteByte
			;    LCDRS_ON(); //Umschalten auf Datenregister
	bsf   lcdport,6
	movf  lcdport,W
    banksel PORTB
	movwf PORTB
			;    Wait();
	movlw .5
	goto  Delay1ms32
			;}
			;
			;//*********************************************
			;// Initialize the LCD module
			;//*********************************************
			;void LCDInit()
			;{
LCDInit
			;    int i;
			;
    IFDEF   HASCMCON    ; 627,628,648,87,88
            banksel CMCON   ;
            errorlevel  -302
            ; CMCON 0:2=111, outputs off, inputs disconnected
            movlw   H'07'       ; Comparators reset
            movwf   CMCON
    ENDIF
    IFDEF   HASCCPCON    ; 87,88,818,819
            banksel CCP1CON   ;
            errorlevel  -302
            ; CCP1CON 0:3=0000 comparator/PWM module off
            clrf    CCP1CON
    ENDIF
    IFDEF   HASANSEL    ; 88
            banksel ANSEL
            errorlevel  -302
            movlw   H'80'       ; All RA's digital
            movwf   ANSEL
    ENDIF
    IFDEF   HASADCON    ; 818,819
            banksel ADCON1
            errorlevel  -302
            ; On 87/88, ADCON1 only affects Vref, 818,819 ADCON1 0:3=011x sets all pins digital
            movlw   0x06
            movwf   ADCON1
    ENDIF
            errorlevel  +302
			;    PORTB=0;
    banksel PORTB
	CLRF  PORTB
			;    LCDRS_OFF();  //Control Register
	bcf   lcdport,6
	movf  lcdport,W
	movwf PORTB
			;    LCDCS_OFF();  //Turn off chip enable
	bcf   lcdport,4
	movf  lcdport,W
	movwf PORTB
			;
			;    //Software Reset according to Hitachi Datasheet
			;    Delay1ms32(255); //Possible Power On Reset wait
	movlw .255
	call  Delay1ms32
			;    for(i=0; i<3; i++)
	CLRF  i
m037	btfsc i,7
	goto  m038
	movlw .3
	subwf i,W
	btfsc STATUS,C
	goto  m039
			;	{
			;	    lcdport=lcdport&0xF0;//D0-D3 mask
m038	movlw .240
	andwf lcdport,F
			;	    lcdport|=0x03;	//Initialize first sybble
	movlw .3
	iorwf lcdport,F
			;	    PORTB=lcdport;	//Send it to the port
	movf  lcdport,W
    banksel PORTB
	movwf PORTB
			;
			;	    LCDCS_ON();	//High Nibble write
	bsf   lcdport,4
	movf  lcdport,W
	movwf PORTB
			;	    NOP;
	NOP  
			;	    LCDCS_OFF(); //Chip select back off
	bcf   lcdport,4
	movf  lcdport,W
	movwf PORTB
			;	    Delay1ms32(100); //A little wait for the reset
	movlw .100
	call  Delay1ms32
			;	}
	incf  i,F
	goto  m037
			;    //Ende Software Reset
			;
			;    // The first write access after the RESET is 8 bits
			;    // an access!! Thus only once, not Low High Nibble write.
			;
			;    // Switch to 4 bit operation
			;    lcdport=0x02;	//4 Bit Operation
m039	movlw .2
	movwf lcdport
			;    PORTB=lcdport;	//Send to port b
	movf  lcdport,W
    banksel PORTB
	movwf PORTB
			;    LCDCS_ON();	// Write high nybble
	bsf   lcdport,4
	movf  lcdport,W
	movwf PORTB
			;    Wait();
	movlw .5
	call  Delay1ms32
			;    LCDCS_OFF();
	bcf   lcdport,4
	movf  lcdport,W
	movwf PORTB
			;
			;    // After insert from Wait() = 5ms
			;    // the LCD announcement always comes to switching on
			;    // on. Without Wait's it showed often nix!
			;
			;    LCDWriteByte(0x28);    //4 Bit Operation,2 Line,5x7 Font
	movlw .40
	call  LCDWriteByte
			;    Wait();
	movlw .5
	call  Delay1ms32
			;    LCDWriteByte(0x14); 	//Cursor Move,Right Shift
	movlw .20
	call  LCDWriteByte
			;    Wait();
	movlw .5
	call  Delay1ms32
			;
			;    // LCDWriteByte(0x0F); 	//Display on, Cursor on ,Cursor blink
			;    // LCDWriteByte(0x0E); 	//Display on, Cursor on ,Cursor no blink
			;    LCDWriteByte(0x0C); 	//Display on, Cursor off ,Cursor no blink
	movlw .12
	call  LCDWriteByte
			;    Wait();
	movlw .5
	call  Delay1ms32
			;
			;    LCDWriteByte(0x06); 	//Increment, Display Freeze
	movlw .6
	call  LCDWriteByte
			;    Wait();
	movlw .5
	call  Delay1ms32
			;    LCDWriteByte(0x02); 	//Cursor Home
	movlw .2
	call  LCDWriteByte
			;    Wait();
	movlw .5
	call  Delay1ms32
			;    LCDRS_ON();		//LCD-Datenregister
	bsf   lcdport,6
	movf  lcdport,W
    banksel PORTB
	movwf PORTB
			;}
	RETURN
	end
	
