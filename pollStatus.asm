			title		'PollStatus - Reset the 1821 and wait until ready'
			subtitle	'Part of 1821PICelR'
			list		b=4,c=132,n=85,x=Off

;	$Revision: 0.6 $  $Date: 2007/05/30 14:22:32 $

			include	Processor.inc
			include	DS1821.inc
			global	PollStatus
			extern	MasterReset
			extern	SendByte,ReadByte
			extern	tempbuf

IO			code

PollStatus

			; Reset the DS1821, then loop reading status until
			; the high bit (DONE) of the returned status is set.

m012
			call	MasterReset

			; Send the command to read the status
			movlw	READSTATUS
			call	SendByte
			; Get the 8-bit result
            ;   Note this little bit of weirdness that ReadByte
            ;   will read 8 or 9 bits depending on what is in W
			movlw	.8
			call	ReadByte
			; Keep doing this until the DSDONE bit is set
			btfss	tempbuf,DSDONE
			goto	m012
			return

			end
