		title		'MasterReset - Reset the DS1821'
		subtitle	'Part of 1821PICel'
		list		b=4,c=132,n=85,x=Off
		;// Send the master reset sequence and query
		;// for presence of sensor

;	$Revision: 0.7 $  $Date: 2007/05/30 13:46:30 $

		include		Processor.inc
		include		DS1821.inc
		global		MasterReset
		extern		Delay100us,Delay10us

IO		code

MasterReset
    ;
    ; DANGER WILL ROBINSON -- this isn't the safest code
    ; in the world.  Because of RMW issues this could cause
    ; real consternation.  Works OK on the PIC-EL, but
    ; be careful if you have other stuff on PORTA
    ;

        ; Pulse the data pin for 600 us
		banksel		PORTA
		bcf   		DQ

		movlw		.6
		call		Delay100us

		bsf			DQ

		call		Delay10us

		; Wait for the pin to be driven low
m014
		btfsc		DQ
		goto		m014

		; and then high
m015
		btfss		DQ
		goto		m015

		; And wait another 600us
m016
		movlw		.6
		goto		Delay100us

		end
