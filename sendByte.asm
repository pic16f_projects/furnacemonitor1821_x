		title		'SendByte - Send a byte to the DS1821'
		subtitle	'Part of 1821PICelR'
		list		b=4,c=132,n=85,x=Off
;
;	Sends a byte to the DS1821.  Enter with the byte to be
;	sent in W.

			;void SendByte(unsigned char by)

;	$Revision: 0.4 $  $Date: 2005/11/20 14:52:00 $


			include	Processor.inc
			include	DS1821.inc
			global	SendByte
			extern	Delay100us,Delay10us

IODAT		udata_ovr
mask		res		1		; Current bit to send
count		res		1		; Loop counter

SHSP		udata_ovr
by			res		1		; Storage for byte to be sent

IO			code

			;void SendByte(unsigned char by)
SendByte
			; Save off the argument in W
			movwf	by
			; Initialize the bit mask
			; mask=0x01;
			movlw	B'00000001'
			movwf	mask

			; Loop once for each bit in the datum.  Each time the mask
			; is shifted and XORed with the ddatum to see whether the
			; current bit it a 0 or 1.  Then the appropriate signal is
			; sent to the DS1821 for the bit.

			; for(count=0; count<8; count++) //8Bits senden
			clrf	count	; Initialize loop counter
m017
			; Check whether loop is done
			movlw	.8
			subwf	count,W
			btfsc	STATUS,C
			goto	m020

			;   if((mask&by)==0) //Ein 0-Bit
			movf	by,W	; Pick up the data byte
			andwf	mask,W	; And pick off the current bit
			xorlw	.0		; Test whether a zero or one bit
			btfss	STATUS,Z
			goto	m018

			; Its a zero bit
			;     DQ=0; //Now wait min 60us
			banksel	PORTA
			bcf		DQ
			;     Delay10us(); 9 times
			call	Delay10us
			call	Delay10us
			call	Delay10us
			call	Delay10us
			call	Delay10us
			call	Delay10us
			call	Delay10us
			call	Delay10us
			call	Delay10us

			goto	m019
m018
			; Its a one bit
			;   else             //Ein 1-Bit
			;    {
			;     DQ=0;
			bcf		DQ
			;      nop         //Wait briefly
			nop
			nop

			;   DQ=1;
m019
			bsf		DQ
			;   Delay100us(1);
			movlw	.1
			call	Delay100us
			;   mask<<=1; //Next Bit - shift mask left
			bcf		STATUS,C
			rlf		mask,F

			incf	count,F		; Next bit
			goto	m017
m020
			return
			end
