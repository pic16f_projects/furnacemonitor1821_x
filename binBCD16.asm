  ; FILE binbcd16.c
			;//Datei: BINBCD16.C
			;// A 16Bit changes binary number in BCD byte
			;//
			;// After the example of Microchip CD
			;// Use, more drueber do not think!
			;//
			;// 01.05.2001
			;// Compiler CC5x
			;
			;// The assembler support from CC5x is under all sow.
			;// MPC and PICC Lite accepted the original routine
			;// without large problems.
			; 
			;// CC5x places itself on if inline.h # included becomes. Assembler
			;// instructions between #asm and #endasm must be written then
			;// absolutely small. The assembler routine by Microchip was not
			;// to be translated direct. Debt was movlw R2/R1/R0. That
			;// did not want to swallow the compiler. Then I have the Inline
			;// instruction movlw(R2); used. There it took contents of R2
			;// instead of the address of R2. In certain way is also correct for
			;// C. movlw(&R2); then success had. To it Microchip is debt.
			;// Those should have rather taken their own instruction to to
			;// load from FSR: (
			;//
			;//The routine was thereafter however in
			;//
			;//#asm
			;//#endasm
			;// C-Instruction
			;//#asm
			;//#endasm
			;// C-Instruction
			;//
			;
			;// carved up and already the call did not go adjBCD strangely enough
			;// any longer. That did not disturb goto the instructions! I
			;// spendiert adjBCD then its own routine and the largest part
			;// completely in Inline assembler wrote.
			;//
			;// As i-i-Tuepfelchen CC5x did not want then addwf the 0,W and movwf 0.
			;// The 0 probably as constant and not as register address interpreted
			;// addwf INDF, w and movwf INDF went then. I.e.: Assembler routines
			;// the addresses for registers to use must be rewritten always in such
			;// a way that it with names to be addressed.

;	$Revision: 0.3 $  $Date: 2005/11/20 01:56:50 $

		include	Processor.inc
		global	BinToBCD16
		global	R0,R1,R2,B0,B1

SHSP		udata_ovr
counter_13	res		1
tmp_3		res		1

            udata
B0          res             1
B1          res             1
R0          res             1
R1          res             1
R2          res             1


			;
			;#include "prozess.h"      /* Der verwendete Prozessor */
			;#include "protos.h"
			;
			;//Input : 16 Bit Wert in B0=LSB,B1=MSB
			;//Output: 8 Bit Globale Register  R0,R1,R2
			;//        R0 = MSB, R2 = LSB
			;unsigned char B0,B1;
			;unsigned char R0,R1,R2;
			;
			;void adjBCD();
			;
			;void BinToBCD16()
			;{
LIB		code
BinToBCD16
			; unsigned char counter;
			; 
			; Carry=0; 
	bcf   STATUS,C
			; counter=16; //16 Bits sind zu bearbeiten
	movlw .16
	movwf counter_13
			; R0=0; R1=0; R2=0;
	clrf  R0
	clrf  R1
	clrf  R2
			;
			;loop16:
			;       //Rotiere B0,B1 bitweise durch Carry in R2,R1,R0
			; rlf(B0,1);
m027	rlf   B0,F
			; rlf(B1,1);
	rlf   B1,F
			; rlf(R2,1);
	rlf   R2,F
			; rlf(R1,1);
	rlf   R1,F
			; rlf(R0,1);
	rlf   R0,F
			; decfSZ(counter,1); //ein Bit weniger
	decfSZ counter_13,F
			; goto adjDEC16;     //BCD Korrektur wenn noch Bits �brig sind
	goto  m028
			; return;            //Sonst raus hier
	return
			;
			;adjDEC16:
			; movlw(&R2); //Adresse von R2
m028	movlw R2
			; movwf(FSR); //als Zeiger f�r INDF Register
	movwf FSR
			; adjBCD();   //BCD Korrektur
	call  adjBCD
			; 
			; movlw(&R1);
	movlw R1
			; movwf(FSR);
	movwf FSR
			; adjBCD();
	call  adjBCD
			;
			; movlw(&R0);
	movlw R0
			; movwf(FSR);
	movwf FSR
			; adjBCD();
	call  adjBCD
			;
			; goto loop16;     //N�chstes von den 16 Bits
	goto  m027
			;}
			;
			;void adjBCD()
			;{
adjBCD
			; unsigned char tmp;
			; 
			;#asm
			;        movlw 0x03     //Untere BCD-Stelle lade W mit 3
	movlw .3
			;	addwf INDF,W      //0x03 addieren zu R0,R1,R2 Ergebnis in W
	addwf INDF,W
			;	movwf tmp     //W zwischenspeichern f�r Test Bit D3
	movwf tmp_3
			;	btfsc tmp,3   // test if bit 3 = 0
	btfsc tmp_3,3
			;	movwf INDF        //war 1, dann W in Ergebnis speichern
	movwf INDF
			;
			;	movlw 0x30     //Das gleiche mit der oberen BCD-Stelle
	movlw .48
			;	addwf INDF,W
	addwf INDF,W
			;	movwf tmp
	movwf tmp_3
			;	btfsc tmp,7   
	btfsc tmp_3,7
			;	movwf INDF    
	movwf INDF
			;#endasm
			;}
	return
			;
			;//Zeigt ein BCD-Byte auf der LCD-Anzeige
			;//oder eine Hexadezimalzahl
			;void ShowBCD(unsigned char x)
			;{

	end
