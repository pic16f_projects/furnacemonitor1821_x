
            include     Processor.inc
            include     DS1821.inc
            radix       DEC

            global      convert2

            extern      _divS16_16
            extern      arg1_8,arg2_8

            extern      sign2,temp2,tempbuf2
            extern      countdeg2,countrem2

SHSP		udata_ovr
i_2         res		    1

PROG        code
convert2:
			;
			;//Zum testen der Anzeige:  To test the display
			;//   sign=-54;   // Ergebnis = -54.09 result
			;//   sign=54;   // Ergebnis = +53.91
			;//   sign=0;     // Ergebnis = -0.09
			;//   countrem2=(long)300;
			;//   countdeg2=(long)512;
			;
			;   //signed integer aus Zweierkomplement machen wenn negativ
            ; two's complement signed integer make if negative
			;   if(sign2<0)
            btfss       sign2,7
            goto        m041
			;    {
			;     sign2=sign2+255;     //Einerkomplement
            decf        sign2,F
			;     sign2++;            //Zweierkomplement
            incf        sign2,F
			;    }
			;
			;   temp2=(long)(sign2); //int zu long
m041
            movf        sign2,W
            movwf       temp2
            clrf        temp2+1
            btfsc       temp2,7
            decf        temp2+1,F
			;
			;   //Temperatur * 100  => -5500 bis 12500
			;   //Schiebt den Wert um zwei Dezimalstellen nach links
			;   //So spare ich mir unten eine Subtraktion und eine Addition
			;   //im Floating Point Format
            ;
            ; Temperature * 100 => -5500 to 12,500 Shifts the value to two
            ; decimal places to the left so I can save down a subtraction and
            ; addition in floating point format
            ;
			;   tempbuf2=temp2;
            movf        temp2,W
            movwf       tempbuf2
            movf        temp2+1,W
            movwf       tempbuf2+1
			;   for(i=0; i<99; i++) temp2+=tempbuf2; //Die einfache Art * 100 zu nehmen
            clrf        i_2
m042
            movlw       .99
            subwf       i_2,W
            btfsc       STATUS,C
            goto        m043

            movf        tempbuf2+1,W
            addwf       temp2+1,F
            movf        tempbuf2,W
            addwf       temp2,F
            btfsc       STATUS,C
            incf        temp2+1,F
            incf        i_2,F
            goto        m042
			;
			;
			;//Wenn countdeg2=0 m��te durch 0 geteilt werden. Abfangen
			;   if(countdeg2>(long)0) //Schade jetzt mu� ich rechnen
            ; If countdeg2 = 0 would have to be divided by 0. interception
            ; if (countdeg2> (long) 0) / / Too bad now I have to expect
m043
            btfsc       countdeg2+1,7
            goto        m046
            movf        countdeg2,W
            iorwf       countdeg2+1,W
            btfsc       STATUS,Z
            goto        m046
			;    {
			;     //countdeg2-countrem2 in integer rechnen Ergebnis in countrem2
			;     countrem2=countdeg2-countrem2;
            movf        countrem2+1,W
            subwf       countdeg2+1,W
            movwf       countrem2+1
            movf        countrem2,W
            subwf       countdeg2,W
            movwf       countrem2
            btfss       STATUS,C
            decf        countrem2+1,F
			;     //Das Ergebnis ist immer positiv
			;
			;     //Ich m�chte zwei Stellen hinter dem Komma haben
			;     //Das Ergebnis der Division w�re aber kleiner 1
			;     //Um wieder FloatingPoint Operation zu vermeiden
			;     //nehme ich den Z�hler * 100
			;     tempbuf2=countrem2;
            movf        countrem2,W
            movwf       tempbuf2
            movf        countrem2+1,W
            movwf       tempbuf2+1
			;     for(i=0; i<99; i++) countrem2+=tempbuf2; //Die einfache Art * 100 zu nehmen
            clrf        i_2
m044
            movlw       .99
            subwf       i_2,W
            btfsc       STATUS,C
            goto        m045
            movf        tempbuf2+1,W
            addwf       countrem2+1,F
            movf        tempbuf2,W
            addwf       countrem2,F
            btfsc       STATUS,C
            incf        countrem2+1,F
            incf        i_2,F
            goto        m044
			;
			;     countrem2=countrem2/countdeg2; //Und teilen
m045
            movf        countrem2,W
            movwf       arg1_8
            movf        countrem2+1,W
            movwf       arg1_8+1
            movf        countdeg2,W
            movwf       arg2_8
            movf        countdeg2+1,W
            movwf       arg2_8+1
            call        _divS16_16
            movf        arg1_8,W
            movwf       countrem2
            movf        arg1_8+1,W
            movwf       countrem2+1
			;    }
			;   else //countdeg2 war 0, dann 100 * 1/2 LSB(=0.5) addieren
            goto        m047
			;    { countrem2=(long)50; } //1/2 LSB
m046
            movlw       .50
            movwf       countrem2
            clrf        countrem2+1
			;
			;   temp2+=countrem2; //Genaue Temperatur ausrechnen
m047
            movf        countrem2+1,W
            addwf       temp2+1,F
            movf        countrem2,W
            addwf       temp2,F
            btfsc       STATUS,C
            incf        temp2+1,F
			;   //LSB = 0.5 * 100 = 50
			;   //Temperatur - LSB ausrechnen
			;   temp2-=(long)50;
            movlw       .50
            subwf       temp2,F
            btfss       STATUS,C
            decf        temp2+1,F
			;
            movf        temp2,W
            movwf       tempbuf2
            movf        temp2+1,W
            movwf       tempbuf2+1
            return
            end
