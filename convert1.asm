
            include     Processor.inc
            include     DS1821.inc
            radix       DEC

            global      convert1

            extern      _divS16_16
            extern      arg1_8,arg2_8

            extern      sign,temp,tempbuf
            extern      countdeg,countrem

SHSP		udata_ovr
i_2         res		    1

PROG        code
convert1:
			;
			;//Zum testen der Anzeige:  To test the display
			;//   sign=-54;   // Ergebnis = -54.09 result
			;//   sign=54;   // Ergebnis = +53.91
			;//   sign=0;     // Ergebnis = -0.09
			;//   countrem=(long)300;
			;//   countdeg=(long)512;
			;
			;   //signed integer aus Zweierkomplement machen wenn negativ
            ; two's complement signed integer make if negative
			;   if(sign<0)
            btfss       sign,7
            goto        m041
			;    {
			;     sign=sign+255;     //Einerkomplement
            decf        sign,F
			;     sign++;            //Zweierkomplement
            incf        sign,F
			;    }
			;
			;   temp=(long)(sign); //int zu long
m041
            movf        sign,W
            movwf       temp
            clrf        temp+1
            btfsc       temp,7
            decf        temp+1,F
			;
			;   //Temperatur * 100  => -5500 bis 12500
			;   //Schiebt den Wert um zwei Dezimalstellen nach links
			;   //So spare ich mir unten eine Subtraktion und eine Addition
			;   //im Floating Point Format
            ;
            ; Temperature * 100 => -5500 to 12,500 Shifts the value to two
            ; decimal places to the left so I can save down a subtraction and
            ; addition in floating point format
            ;
			;   tempbuf=temp;
            movf        temp,W
            movwf       tempbuf
            movf        temp+1,W
            movwf       tempbuf+1
			;   for(i=0; i<99; i++) temp+=tempbuf; //Die einfache Art * 100 zu nehmen
            clrf        i_2
m042
            movlw       .99
            subwf       i_2,W
            btfsc       STATUS,C
            goto        m043

            movf        tempbuf+1,W
            addwf       temp+1,F
            movf        tempbuf,W
            addwf       temp,F
            btfsc       STATUS,C
            incf        temp+1,F
            incf        i_2,F
            goto        m042
			;
			;
			;//Wenn countdeg=0 m��te durch 0 geteilt werden. Abfangen
			;   if(countdeg>(long)0) //Schade jetzt mu� ich rechnen
            ; If countdeg = 0 would have to be divided by 0. interception
            ; if (countdeg> (long) 0) / / Too bad now I have to expect
m043
            btfsc       countdeg+1,7
            goto        m046
            movf        countdeg,W
            iorwf       countdeg+1,W
            btfsc       STATUS,Z
            goto        m046
			;    {
			;     //countdeg-countrem in integer rechnen Ergebnis in countrem
			;     countrem=countdeg-countrem;
            movf        countrem+1,W
            subwf       countdeg+1,W
            movwf       countrem+1
            movf        countrem,W
            subwf       countdeg,W
            movwf       countrem
            btfss       STATUS,C
            decf        countrem+1,F
			;     //Das Ergebnis ist immer positiv
			;
			;     //Ich m�chte zwei Stellen hinter dem Komma haben
			;     //Das Ergebnis der Division w�re aber kleiner 1
			;     //Um wieder FloatingPoint Operation zu vermeiden
			;     //nehme ich den Z�hler * 100
			;     tempbuf=countrem;
            movf        countrem,W
            movwf       tempbuf
            movf        countrem+1,W
            movwf       tempbuf+1
			;     for(i=0; i<99; i++) countrem+=tempbuf; //Die einfache Art * 100 zu nehmen
            clrf        i_2
m044
            movlw       .99
            subwf       i_2,W
            btfsc       STATUS,C
            goto        m045
            movf        tempbuf+1,W
            addwf       countrem+1,F
            movf        tempbuf,W
            addwf       countrem,F
            btfsc       STATUS,C
            incf        countrem+1,F
            incf        i_2,F
            goto        m044
			;
			;     countrem=countrem/countdeg; //Und teilen
m045
            movf        countrem,W
            movwf       arg1_8
            movf        countrem+1,W
            movwf       arg1_8+1
            movf        countdeg,W
            movwf       arg2_8
            movf        countdeg+1,W
            movwf       arg2_8+1
            call        _divS16_16
            movf        arg1_8,W
            movwf       countrem
            movf        arg1_8+1,W
            movwf       countrem+1
			;    }
			;   else //countdeg war 0, dann 100 * 1/2 LSB(=0.5) addieren
            goto        m047
			;    { countrem=(long)50; } //1/2 LSB
m046
            movlw       .50
            movwf       countrem
            clrf        countrem+1
			;
			;   temp+=countrem; //Genaue Temperatur ausrechnen
m047
            movf        countrem+1,W
            addwf       temp+1,F
            movf        countrem,W
            addwf       temp,F
            btfsc       STATUS,C
            incf        temp+1,F
			;   //LSB = 0.5 * 100 = 50
			;   //Temperatur - LSB ausrechnen
			;   temp-=(long)50;
            movlw       .50
            subwf       temp,F
            btfss       STATUS,C
            decf        temp+1,F
			;
            movf        temp,W
            movwf       tempbuf
            movf        temp+1,W
            movwf       tempbuf+1
            return
            end
