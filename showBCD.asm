;	ShowBCD
;
;	Enter with a BCD byte in the W register.  Displays the
;	two characters on the LCD starting in the current
;	LCD cursor position.

			;//Zeigt ein BCD-Byte auf der LCD-Anzeige
			;//oder eine Hexadezimalzahl

;	$Revision: 0.3 $  $Date: 2005/11/20 02:01:37 $

		include	  Processor.inc
		global	  ShowBCD
		extern	  R0,R1,R2,B0,B1
		extern	  LCDWriteByte

SHSP		udata_ovr
;x				EQU 0x0D
;nib			EQU 0x0E
x		res     1
nib		res     1
					; FILE binbcd16.c
PROG	code
					;
					;void ShowBCD(unsigned char x)
					;{
ShowBCD
		movwf	x
					; unsigned char nib;
					;
					; nib=(x>>4); //High-Nibble
		swapf	x,W
		andlw	.15
		movwf	nib
					; if(nib<10) nib+=0x30; //ASCII Zahlen
		movlw	.10
		subwf	nib,W
		btfsc	STATUS,C
		goto	m029
		movlw	.48
		addwf	nib,F
					; else nib+=0x37;  //ASCII Gro;buchst.
		goto	m030
m029	movlw	.55
		addwf	nib,F
					; LCDWriteByte(nib);
m030	movf	nib,W
		call	LCDWriteByte
					; nib=x&0x0F; //Low-Nibble
		movlw	.15
		andwf	x,W
		movwf	nib
					; if(nib<10) nib+=0x30; //ASCII Zahlen
		movlw	.10
		subwf	nib,W
		btfsc	STATUS,C
		goto	m031
		movlw	.48
		addwf	nib,F
					; else nib+=0x37;  //ASCII Gro;buchst.
		goto	m032
m031	movlw	.55
		addwf	nib,F
					; LCDWriteByte(nib);
m032	movf	nib,W
		goto	LCDWriteByte
					;}
		end
